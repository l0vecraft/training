# $ pylint fgomezoso.py
#
# --------------------------------------------------------------------
# Your code has been rated at 10.00/10 (previous run: 10.00/10, +0.00)

"""
This program calculates if 2 different strings can be equal by swapping
2 characters from both strings exactly ONCE.

When 1 swap isn't enough then it is not possible to make them equal.
"""
from __future__ import print_function


def main():

    """
    Check if both strings has exactly 2 unmatched characters.
    If it's true, the only way to making them equal is when the 2
    characters of the same string are equal, so 1 swap is possible.
    """

    data = open("DATA.lst", "r")

    tests = int(data.readline().rstrip('\n'))

    result = []

    for _ in range(tests):

        length = int(data.readline().rstrip('\n'))

        string1 = data.readline().rstrip('\n')
        string2 = data.readline().rstrip('\n')

        no_matches = 0

        str1_unmatched = []
        str2_unmatched = []

        for k in range(length):

            if string1[k] != string2[k]:

                no_matches += 1

                str1_unmatched.append(string1[k])
                str2_unmatched.append(string2[k])

        if no_matches == 2:

            if (str1_unmatched[0] == str1_unmatched[1]
                    and str2_unmatched[0] == str2_unmatched[1]):

                result.append("Yes")

            else:

                result.append("No")

        else:

            result.append("No")

    for item in result:

        print(item)

    data.close()


main()

# $ python fgomezoso.py
# Yes
# No
# No
# No
