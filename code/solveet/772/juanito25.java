//Tabla de multiplicar de n numero

import java.util.Scanner;

public class Reto4 {
    public static void main(String[] args) {
  
        Scanner digit = new Scanner(System.in);
        int numero    = 0;

        System.out.println("");
        System.out.println("*******");
        System.out.println("*Reto4*");
        System.out.println("*******");   

        System.out.println("");
        System.out.println("**********************************");
        System.out.println("*Tabla de multiplicar de n numero*");
        System.out.println("**********************************");   
        System.out.println("");

        System.out.print("Ingrese por favor un numero: ");
        numero = digit.nextInt();
        System.out.println("");

        for(int i = 1; i<=10; i++) {
            System.out.println(numero + " * " + i + " = " + numero*i );
        }
    }    
}
