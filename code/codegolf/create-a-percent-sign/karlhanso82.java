/*
$pmd.bat -d karlhanso82.java -R rulesets/java/quickstart.xml -f text #linting
may 15, 2019 9:35:03 AM net.sourceforge.pmd.PMD processFiles
WARNING:
This analysis could be faster, please consider using
Incremental  Analysis:
https://pmd.github.io/pmd-6.14.0/pmd_userdocs_incremental_analysis.html
$ javac -d . karlhanso82.java #Compilation
*/
import java.io.*;
import java.net.URL;
import java.util.*;
import java.util.Properties;
import java.net.URL;

class Percentsing {

 URL path;
 InputStream input;
 static Properties prop;

 public Percentsing() throws Exception {
  path = ClassLoader.getSystemResource("DATA.lst");
  input = new FileInputStream(path.getFile());
  prop = new Properties();
  prop.load(input);
 }

 public void resultante(String m) {
  int n = Integer.parseInt(m);
  if (prop.getProperty("value").equals(Integer.toString(n))) {
   String[] res = new String[n + 1];
   res[Integer.parseInt(prop.getProperty("fv"))] = prop.getProperty("value");
   res[Integer.parseInt(prop.getProperty("value"))
   ] = prop.getProperty("value");
   printarr(res);
  } else {
   String[][] res2 = new String[n][n];
   int fv = Integer.parseInt(prop.getProperty("fv"));
   res2 = fillarr(res2);
   res2[fv][fv] = prop.getProperty("value");
   res2[n - 1][n - 1] = prop.getProperty("value");
   res2 = filldiagonal(res2);
   printarrmul(res2);
  }
 }

 public void printarr(String[] a) {
  for (int i = 0; i < a.length; i++) {
   System.out.println(a[i]);
  }
 }

 public void printarrmul(String[][] a) {
  for (int i = 0; i < a.length; i++) {
   for (int j = 0; j < a[i].length; j++) {
    System.out.print(a[i][j]);
   }
   System.out.println();
  }
 }

 public String[][] fillarr(String[][] b) {
  for (int i = 0; i < b.length; i++) {
   for (int j = 0; j < b[i].length; j++) {
    b[i][j] = prop.getProperty("value1");
   }
  }
  return b;
 }

 public String[][] filldiagonal(String[][] b) {
  int col = b[0].length - 1;
  for (int i = 0; i < b.length; i++) {
   for (int j = 0; j < b[i].length; j++) {
    if (j == col) {
     b[i][j] = prop.getProperty("value");
     col--;
    }
   }
  }
  return b;
 }

 public static void main(String arg[]) throws Exception {

  Percentsing p = new Percentsing();
  p.resultante(prop.getProperty("n1"));
  System.out.println();
  p.resultante(prop.getProperty("n2"));
  System.out.println();
  p.resultante(prop.getProperty("n3"));
  System.out.println();
  p.resultante(prop.getProperty("n4"));
  System.out.println();
  p.resultante(prop.getProperty("n5"));
 }
}
/*
$ java karlhanso82
Create a percent sign
$
*/

