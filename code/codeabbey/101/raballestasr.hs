-- | Codeabbey 101: Gradient Calculation
-- | http://www.codeabbey.com/index/task_view/gradient-calculation
-- | **** Compilation
-- | $ ghc -dynamic raballestasr.hs
-- | [1 of 1] Compiling Main             ( raballestasr.hs, raballestasr.o )
-- | Linking raballestasr ...
-- | **** Test run (with easy DATA.lst)
-- | $ ./raballestasr
-- | "222 246 211 212 234 254 19 214 172 234 244 254 175 213"
-- | **** Linter output
-- | $ hlint raballestasr.hs
-- | No hints

-- | Computes the gradient direction in degrees at point p
-- | with parameters a,b,c.
slope_degs :: Float -> Float -> Float -> [Float] -> Int
slope_degs a b c point =
    round $ (360 * (atan2 fy fx + pi)) / (2*pi)
    where   fy = 2*(y-b) - 2*c*(y+b)*ex
            fx = 2*(x-a) - 2*c*(x+a)*ex
            ex = exp ( - (x+a)**2 - (y+b)**2 )
            x = head point
            y = last point

main = do
    readr <- readFile "DATA.lst"
    let splat = map words (lines readr)
    let [_, s, t, v] = map (\x -> read x ::Float) (head splat)
    print $ unwords $
      map ( show . slope_degs s t v . map (\x -> read x ::Float) ) (tail splat)
