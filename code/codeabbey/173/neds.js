/*
$ eslint neds
$ node neds
*/

const fsystem = require('fs');

const FULL_OCTAVE = 12;
const FIFTH_STEPS = 7;
const GTHIRD_STEPS = 4;
const LTHIRD_STEPS = 3;
const filePath = 'DATA.lst';
const midi = {
  0: 'C',
  1: 'C#',
  2: 'D',
  3: 'D#',
  4: 'E',
  5: 'F',
  6: 'F#',
  7: 'G',
  8: 'G#',
  9: 'A',
  10: 'A#',
  11: 'B',
};

let type = '';

function checkType(note, lth, gth) {
  if (note === lth) {
    type = 'minor';
  } else if (note === gth) {
    type = 'major';
  }
  return type;
}

function octaves(note) {
  let not = parseInt(note, 10);
  while (not >= FULL_OCTAVE) {
    not -= FULL_OCTAVE;
  }
  return not;
}

function checkSize(note) {
  if (note >= FULL_OCTAVE) {
    note -= FULL_OCTAVE;
  }
  return note;
}

function chords(chord) {
  const notes = chord.split(' ');
  let cont = 0;
  let root = 0;
  let sound = false;
  let typ = '';
  let out = '';

  while (cont < notes.length) {
    root = octaves(notes[cont]);
    for (let i = 0; i < notes.length; i++) {
      const note = octaves(notes[i]);
      let fifth = root + FIFTH_STEPS;
      let lth = root + LTHIRD_STEPS;
      let gth = root + GTHIRD_STEPS;

      fifth = checkSize(fifth);
      lth = checkSize(lth);
      gth = checkSize(gth);

      if (note === fifth || note === lth || note === gth || note === root) {
        sound = true;
        typ = checkType(note, lth, gth);
      } else {
        sound = false;
        typ = '';
        break;
      }
    }

    if (sound) {
      out = midi[root];
      out += '-';
      out += typ;
      out += ' ';
      process.stdout.write(out);
      sound = false;
      break;
    } else if (cont === notes.length - 1) {
      process.stdout.write('other ');
      break;
    }
    cont++;
  }
}

function readData(dat) {
  const lines = dat.split('\n');
  for (let i = 1; i < lines.length - 1; i++) {
    chords(lines[i]);
  }
}

function main() {
  return fsystem.readFile(filePath, (iss, dat) => {
    if (iss) {
      return iss;
    }
    return readData(dat.toString());
  });
}

main();

/*
$ node neds
G#-minor A#-major C-major D#-minor other F-minor E-minor other A-minor B-major
A#-minor D-minor other F-minor A-minor E-major F-major other other G-minor
other G-major E-major B-major other G#-minor G-minor G-major other G-major
other D-minor F-major
*/
