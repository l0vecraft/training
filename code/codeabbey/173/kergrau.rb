# rubocop kergrau.rb
# Inspecting 1 file
# .
#
# 1 file inspected, no offenses detected
require 'matrix'
answer = ''
flag = false
i = -1
matrix = Matrix.build(11, 12) { i += 1 }

# Validate if a note belongs to chord
def val_note(num_col, matrix, num_index)
  col = num_col > 11 ? num_col - 12 : num_col
  matrix.column(col).to_a.index(num_index).nil?
end

file = File.new('DATA.lst', 'r')
while (line = file.gets)
  unless flag
    flag = true
    next
  end

  the_third = false
  the_fourth = false
  the_fifth = false
  root = -1

  # Find the root and determine the chord
  line.split(' ').each do |roots|
    _row, root = matrix.index(roots.to_i)
    the_third = false
    the_fourth = false
    the_fifth = false

    line.split(' ').each do |note|
      unless val_note(root + 3, matrix, note.to_i)
        the_third = true
        next
      end
      unless val_note(root + 4, matrix, note.to_i)
        the_fourth = true
        next
      end
      unless val_note(root + 7, matrix, note.to_i)
        the_fifth = true
        next
      end
    end
    break if (the_third || the_fourth) && the_fifth
  end
  # Put the name to chord
  if (the_third || the_fourth) && the_fifth
    maj_min = the_third == true ? 'minor' : 'major'
    case root
    when 0 then answer << "C-#{maj_min} "
    when 1 then answer << "C#-#{maj_min} "
    when 2 then answer << "D-#{maj_min} "
    when 3 then answer << "D#-#{maj_min} "
    when 4 then answer << "E-#{maj_min} "
    when 5 then answer << "F-#{maj_min} "
    when 6 then answer << "F#-#{maj_min} "
    when 7 then answer << "G-#{maj_min} "
    when 8 then answer << "G#-#{maj_min} "
    when 9 then answer << "A-#{maj_min} "
    when 10 then answer << "A#-#{maj_min} "
    when 11 then answer << "B-#{maj_min} "
    end
  else
    answer << 'other '
  end
end
puts answer

# ruby kergrau.rb
# G#-minor A#-major C-major D#-minor other F-minor E-minor other A-minor
# B-major A#-minor D-minor other F-minor A-minor E-major F-major other other
# G-minor other G-major E-major B-major other G#-minor G-minor G-major other
# G-major other D-minor F-major
