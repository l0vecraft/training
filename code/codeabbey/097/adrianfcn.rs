/*
  $ rustfmt adrianfcn.rs
  $ rustc --test adrianfcn.rs
running 0 tests

test result: ok. 0 passed; 0 failed; 0 ignored; 0 measured; 0 filtered out
  $ rustc adrianfcn.rs
*/
use std::fs::File;
use std::io::prelude::*;

fn solution(inputs : &Vec<i64>) {
  let legs = inputs[0];
  let breast = inputs[1];
  let mut sol = 0;
  let mut i = 1;
  while 4*i < legs {
    let j = legs - i*4;
    let div = breast - j;
    if div % i == 0 && (div / i) % 2 == 0 {
      sol += 1;
    }
    i += 1;
  }
  print!("{} ", sol);
}

fn main() -> std::io::Result<()> {
  let mut file = File::open("DATA.lst")?;
  let mut contents = String::new();
  file.read_to_string(&mut contents)?;
  let mut lines = contents.lines();
  lines.next();
  for l in lines {
    let mut inputs = vec![];
    for n in l.split_whitespace() {
      let a = n.parse::<i64>().unwrap();
      inputs.push(a)
    }
    solution(&inputs);
  }
  println!();
  Ok(())
}
/*
  $ ./adrianfcn
  11 12 11 12 3 8 2 3 8 5 11 13 6 3 6
*/
