/*
$ rustfmt santiyepes.rs --write-mode=diff
$
$ rustc santiyepes.rs
$
*/
use std::fs::File;
use std::io::{BufRead, BufReader};

fn main() {
  let file = File::open("DATA.lst").unwrap();

  for buff in BufReader::new(file).lines() {
    let mut line: &str = &buff.unwrap();
    let mut array: Vec<&str> = line.split(' ').collect();
    let mut numbers: Vec<i64> = Vec::new();

    for x in &array {
      let mut string_number: String = x.to_string();
      let mut number_conver: i64 = string_number.parse::<i64>().unwrap();
      numbers.push(number_conver);
    }
    if numbers.len() > 1 {
      if numbers[0] < numbers[1] {
        print!("{} ", numbers[0]);
      } else {
        print!("{} ", numbers[1]);
      }
    }
  }
}

/*
$ ./santiyepes
-2605178 -8641437 -1673153 -8560512 391090 -6006084
-446852 -7897953 5695945 6727636 -6232151 320213
-8684553 -7369740 -4312186 3082635 -9672707 -7403785
-1345861 -9665427-8571476 -5498134 1971156
3278711 1395820 1187365 -6111861-4366784
*/
