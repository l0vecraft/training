###
$ coffeelint --reporter jslint danmur97.coffee #linting
$ coffee -c danmur97.coffee #compilation
###

class PriorityQueue
  constructor: (@queue)->
  add: (element)->
    i = 0
    while i < @queue.length && element.getPriority() > @queue[i].getPriority()
      i++
    if i >= @queue.length
      @queue.push(element)
    else
      @queue.splice(i, 0, element)
    0
  shift: ()->
    @queue.shift()
  length: ()->
    @queue.length
  getQueue: ()->
    @queue
  trace: ()->
    console.log(this)
    for k,v of @queue
      v.getObj().trace()

class PriorityQElement
  constructor: (@obj,@priority)->
  getObj: ()->
    @obj
  getPriority: ()->
    @priority

class TreeNode
  constructor: (@element)->
    @parent = null
    @children = []
  getChildren: ()->
    @children
  getElement: ()->
    @element
  setParent: (parent)->
    @parent = parent
  addChild: (child)->
    @children.push(child)
    child.setParent(this)
  trace: ()->
    console.log(this)

escapeRegExp = (text)->
  text.replace(/[-[\]{}()*+?.,\\^$|#\s]/g, '\\$&')

count_letters = (word)->
  count={}
  for i in [0...word.length]
    if(!count[word[i]])
      count[word[i]]=1
    else
      count[word[i]]++
  count

class HuffmanCoding
  constructor: ()->
    @queue = new PriorityQueue []
  generateTree: (text)->
    count = count_letters(text)
    for k,v of count
      node = new TreeNode k
      element = new PriorityQElement node,v
      @queue.add(element)
    @queue
  compressTree: (text)->
    this.generateTree(text)
    while @queue.length()>1
      a = @queue.shift();b = @queue.shift() # PriorityQElement
      aNode = a.getObj();bNode = b.getObj() # TreeNode
      node = new TreeNode aNode.getElement()+bNode.getElement()
      node.addChild(aNode);node.addChild(bNode)
      pElement = new PriorityQElement node,a.getPriority()+b.getPriority()
      @queue.add(pElement)
  compressCoding: (text)->
    this.compressTree(text)
    rootNode = @queue.getQueue()[0].getObj()
    chars = rootNode.getElement().match(/.{1}/g)
    coding = {}
    for k,v of chars
      coding[v] = this.letterCoding(v,rootNode)
    coding
  compressRatio: (text,coding)->
    count = count_letters(text)
    compressSize = 0
    for k,v of count
      compressSize += coding[k].length*v
    text.length*8/compressSize
  letterCoding: (char,node)->
    children = node.getChildren()
    code = ''
    for k,v of children
      element = v.getElement()
      regPattern = new RegExp(escapeRegExp(char),'g')
      if element.match(regPattern)
        if element.length > 1
          code = k + this.letterCoding(char,v)
        else
          code = k
    code

main = (x)->
  HC = new HuffmanCoding
  text = x[2]
  coding = HC.compressCoding(text)
  ratio = HC.compressRatio(text,coding)
  console.log(ratio)
  0

main(process.argv)

###
$ coffee danmur97.coffee "$(< DATA.lst)"
1.9180201082753288
###
