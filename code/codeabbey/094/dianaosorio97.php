<?php
/**
 * Fool's days 2014
 *
 * PHP version 7.2.10
 * phpcs dianaosorio97.php
 *
 * @category Challenge
 * @package  Suma
 * @author   Diana Osorio <dianaosorio66@gmail.com>
 * @license  GNU General Public License
 * @link     none
 */
if (!function_exists('suma')) {
    if (file_exists("DATA.lst")) {
        $file = fopen("DATA.lst", "r");
        $cant = fgets($file, 128);
        for ($i=0; $i < $cant; $i++) {
            $num = explode(' ', fgets($file, 128));
            $result = 0;
            foreach ($num as $k) {
                $result += pow((int)$k, 2);
            }
            print_r($result);
            echo " ";
        }
        fclose($file);
    } else {
        echo "Error";
    }
}

/*
php dianaosorio97.php
output:
1649 860 1527 10 45 29 983 1135 1010 40
84 467 838 920 209 1213 578 261
*/
?>
