/*
  $ eslint adrianfcn.js
*/
function solution(input) {
  const values = input;
  let sol = '';
  for (let jos = 1; jos < values.length; jos++) {
    let cont = 0;
    const box = values[jos];
    for (let i = jos; i > 0; i--) {
      if (values[i - 1] > box) {
        values[i] = values[i - 1];
        values[i - 1] = box;
        cont++;
      }
    }
    sol = `${ sol }${ cont } `;
  }
  const output = process.stdout.write(`${ sol }\n`);
  return output;
}

function inputs(unt, cont) {
  const contents = cont.split('\n');
  contents.pop();
  contents.shift();
  const input = contents.join(' ')
    .split(' ')
    .map((value) => (parseInt(value, 10)));
  solution(input);
}

const file = require('fs');
function fileLoad() {
  return file.readFile('DATA.lst', 'utf8', (mistake, contents) =>
    inputs(mistake, contents)
  );
}

fileLoad();
/*
  $ node adrianfcn.js
  1 0 2 2 0 3 0 0 3 0 8 3 13 11 1 11 9 7 2 15 10 19 8 8 15 17 25 22 4 26 7
  17 19 24 33 27 29 19 36 30 13 40 3 18 19 8 9 46 11 25 10 9 17 17 0 33 18
  31 55 14 49 1 40 26 58 32 62 23 2 7 40 72 7 15 20 51 31 35 1 61 51 4 24
  76 27 18 33 22 56 13 46 24 17 35 0 68 88 67 43 55 75 98 47 4 93 94 8 53
  106 24 91 26 63 95 30 34 88 64 89 94 82 92 113 121 27 38 88 88
*/
