open System

[<EntryPoint>]
let main argv =
    let secuence a b n =
        let mutable sum = 0
        for i in 0..(n-1) do
            sum <- sum + (a + i*b)
        sum
    printfn "ingrese la cantidad de secuencias:"
    let c = stdin.ReadLine() |> int
    let mutable out = Array.zeroCreate c
    for i in 0.. (c-1) do
        printfn "(%i) ingrese separados por un espacio A: primer termino B: incremento N: numero de terminos" (i+1)
        let mutable arrayData = stdin.ReadLine().Split ' '
        out.[i] <- secuence (int arrayData.[0]) (int arrayData.[1]) (int arrayData.[2])
    for i in 0..(out.Length - 1) do
        printf "%i " out.[i]
    Console.ReadKey() |> ignore
    0 // devolver un codigo de salida entero
