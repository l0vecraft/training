/*
$ dub run dscanner -- -S simongomez95.d
Building package dscanner in /Users/nekothecat/.dub/packages/dscanner-0.5.11/ds
canner/
Running pre-generate commands for dscanner...
Performing "debug" build using /Library/D/dmd/bin/dmd for x86_64.
stdx-allocator 2.77.2: target for configuration "library" is up to date.
emsi_containers 0.8.0-alpha.9: target for configuration "unittest" is up to dat
e.
libdparse 0.9.8: target for configuration "library" is up to date.
dsymbol 0.4.8: target for configuration "library" is up to date.
inifiled 1.3.1: target for configuration "library-quiet" is up to date.
libddoc 0.4.0: target for configuration "lib" is up to date.
dscanner 0.5.11: building configuration "application"...
Linking...
To force a rebuild of up-to-date targets, run again with --force.
Running ../../../../../.dub/packages/dscanner-0.5.11/dscanner/bin/dscanner -S s
imongomez95.d

$ dmd simongomez95.d
*/

import std.stdio;
import std.file;
import std.string;
import std.conv;
import std.algorithm;

void main() {
  File file = File("DATA.lst", "r");
  string countstring = file.readln();
  countstring = stripws(countstring);
  const int count = to!int(countstring);
  string answer;
  int first, step, noval, sum;
  for(int i=0; i<count; i++) {
    string[] numberstring = file.readln().split(" ");
    first = to!int(stripws(numberstring[0]));
    step = to!int(stripws(numberstring[1]));
    noval = to!int(stripws(numberstring[2]));
    sum = first;
    for(int j=1; j<noval; j++) {
      sum = sum + (first + j*step);
    }
    answer = answer ~ " " ~ to!string(sum);
  }
  writeln(answer);
  file.close();
}

private string stripws(string str) {
  str = strip(str, " ");
  str = strip(str, "\n");
  str = strip(str, "  ");
  return str;
}

/*
$ ./simongomez95
 91180 49039 29762 9860 3552 867 97119 3225 779 3654
*/
