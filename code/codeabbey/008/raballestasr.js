"use strict";

/**
 * Gives the sum of an arithmetic progression.
 * @param {number} start The initial term in the progression.
 * @param {number} step The increment of the progression.
 * @param {number} number The number of terms to be added.
 * @return {number} The sum of the terms in the progression.
*/
function sumArithProg(start, step, number) {
    return number * start + step * number * (number - 1) / 2;
}

var fs = require("fs");
var lines = fs.readFileSync("DATA.lst").toString().split("\n");
var salida = "";
var i;
var line;
for (i = 1; i < lines.length; i += 1) {
    line = lines[i].split(" ");
    salida += sumArithProg(Number(line[0]), Number(line[1]),
        Number(line[2])) + " ";
}
console.log(salida);
