/*
$ eslint badwolf10.js #linting
$ node badwolf10.js #compilation
*/

// Unnecesary or conflicting rules
/* eslint-disable filenames/match-regex */
/* eslint-disable fp/no-unused-expression */
/* eslint-disable no-console */
/* eslint-disable array-bracket-newline */
/* eslint-disable array-element-newline */

function computeMean(xydata, accmean, ndata) {
  if (xydata.length === 0) {
    return accmean / ndata;
  }
  const naccmean = accmean + xydata[0];
  return computeMean(xydata.slice(1), naccmean, ndata);
}

function computeVariance(xdata, meanx, accvarx) {
  if (xdata.length === 0) {
    return accvarx;
  }
  const naccvarx = accvarx + Math.pow((xdata[0] - meanx), 2);
  return computeVariance(xdata.slice(1), meanx, naccvarx);
}

function computeCovariance(mdata, xmean, ymean, accovar) {
  if (mdata.length === 0) {
    return accovar;
  }
  const naccovar = ((mdata[0][0] - xmean) * (mdata[0][1] - ymean)) + accovar;
  return computeCovariance(mdata.slice(1), xmean, ymean, naccovar);
}

function computeLinearRegression(mdata) {
  const meanx = computeMean(mdata.map((ldata) => ldata[0]), 0, mdata.length);
  const meany = computeMean(mdata.map((ldata) => ldata[1]), 0, mdata.length);
  const varx = computeVariance(mdata.map((ldata) => ldata[0]), meanx, 0);
  const covxy = computeCovariance(mdata, meanx, meany, 0);
  const slope = covxy / varx;
  const intercept = meany - (slope * meanx);
  return [ slope, intercept ];
}


function findLinearRegression(readerr, contents) {
  if (readerr) {
    return readerr;
  }
  const dataLines = contents.split('\n');
  const mdata = dataLines.slice(1).map((line) =>
    line.split(' ').slice(1).map(Number));
  const linear = computeLinearRegression(mdata);
  linear.map((val) => console.log(val));
  return 0;
}

const filesys = require('fs');
function main() {
  return filesys.readFile('DATA.lst', 'utf8', (readerr, contents) =>
    findLinearRegression(readerr, contents));
}

main();

/*
$ node badwolf10.js
1.5405377931602007
107.31285427305949
*/
