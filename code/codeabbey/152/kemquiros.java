/*
 * Author: Kemquiros
 * Purpose: Solve the 152th CodeAbbey problem.
 * Language:  Java
 */
package kemquiros;

import java.math.BigInteger;
import java.util.Scanner;

public class Kemquiros {

    public static void main(String[] args) {
        BigInteger p, q, n, phi, a, d;
        BigInteger e = new BigInteger("65537");//Constant value
        BigInteger cipher;
        String message;
        Scanner sc = new Scanner(System.in);
        //Read data
        p = sc.nextBigInteger();
        q = sc.nextBigInteger();
        cipher = sc.nextBigInteger();

        n = p.multiply(q); // n = p*q       
        phi = phi(n, p, q);//Calculates phi(n)
        d = e.modInverse(phi); //Calculates inverse module phi(n)         

        //Decrypt
        a = cipher.modPow(d, n);
   
        message = a.toString();
        message = message.split("00")[0];
        for(int i=0;i<message.length()-1;i+=2){
            System.out.print((char)Integer.parseInt(message.substring(i, i+2)));
        }
        System.out.print("\n");
    }

    public static BigInteger phi(BigInteger n, BigInteger p, BigInteger q) {
        //phi(n) = n - p - q + 1
        BigInteger temp;
        temp = n.subtract(p);
        temp = temp.subtract(q);
        temp = temp.add(new BigInteger("1"));
        return temp;
    }

}
