#!/usr/bin/env python3
"""
Problem #6 Rounding
"""
class Rounding:
    """
    Rounding values, when certain value contains exactly 0.5
    as a fraction part, value should be rounded.
    """
    FRACTION_PART = 0.5
    values, result = [], []
    division = 0.0

    while True:

        line = input()

        if line:
            if len(line) < 3:
                pass
            else:
                values = line.split()
                division = float(values[0]) / float(values[1])
                if division > 0:
                    division += FRACTION_PART
                else:
                    division -= FRACTION_PART
                division = int(division)
                result.append(str(division))
        else:
            break

    text = ' '.join(result)
    print(text)

Rounding()
