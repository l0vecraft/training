#=
 $julia
 julia> using Lint
 julia> lintfile("kergrau.jl")
=#
using Primes

open("DATA.lst") do file
  flag = false
  index = 0

  for ln in eachline(file)
    if flag == false
      flag = true
      continue
    end
    for i in split(ln, " ")
      index = parse(Int64, i)
      n_prime = primes(2750159)
      println(n_prime[index])
      index = 0
    end
  end
end

# $julia kergrau.jl
# 1521991 2635883 1340153 1648253 2043817 2386247 1986889 1540073 2544229
# 1940201 2464349 1724861
