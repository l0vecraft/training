#!/usr/bin/env python
"""
 Problem #1 Sum "A+B"
"""
class SumOfTwo:
    """
        Input data of two numbers to sum.
    """
    test_case = raw_input().split()
    print int(test_case[0])+int(test_case[1])

SumOfTwo()
