%{
   mlint('juanmusic1.m','-config=factory') # init linter config
   mcc -m juanmusic1.m #compile to matlab
%}

x = fileread ("DATA.lst");
form = '%f %f'; % we have 2 columns, then use 2 %f
out = textscan(x, form);

y = cell2mat(out);

result = y(1,1) + y(1,2);
disp(result)

%{
   matlab -nodisplay -nosplash -nodesktop -r "run('juanmusic1.m');exit;"
   18772
%}
