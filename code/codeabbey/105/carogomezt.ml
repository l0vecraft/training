let n = Scanf.scanf "%d\n"(fun n ->  n);;

let printArray r = Array.iter (Printf.printf "%d ") r;;
let x = Array.make (n+1) 0;;
let y = Array.make (n+1) 0;;

let convex_poligon_area x y = let sum = ref 0 in
                            let i = ref 0 in
                            let area = ref 0.0 in
                            while !i < (Array.length x)-1 do
                                sum := !sum + ((x.(!i) * y.(!i+1)) - x.(!i+1) * y.(!i));
                                i := !i + 1;
                            done;
                            area := float(!sum) /. 2.0;
                            abs_float(!area);;

for i = 0 to n-1 do
    let values = read_line () in
    let intlist = List.map int_of_string(Str.split (Str.regexp " ") values) in
    let xi = List.nth intlist 0 in
    let yi = List.nth intlist 1 in
    x.(i) <- xi;
    y.(i) <- yi;
done;;

x.(n) <- x.(0);;
y.(n) <- y.(0);;

let result = convex_poligon_area x y;;
Printf.printf "%F" result;;
