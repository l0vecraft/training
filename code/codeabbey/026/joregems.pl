#$perl -Mstrict -Mdiagnostics -cw joregems.pl
#joregems.pl syntax OK
use List::Util qw[min max];
use List::Util qw/sum/;
use warnings;
use strict;
my $data = 'DATA.lst'; #the variable associated to the file
#that contains the numbers
my $skipHeader=0; #variable for skip the first line of the file
open(FH, '<', $data) or die $!; #open the file with the numbers
while(<FH>){ #go through the file line by line
  my @numbersArray; #the numbers in array version
  my @list = split(' ', $_); #splitting the line of the file to make it
  #manageable
  foreach my $value (@list){#go through numbersArray for make operations
    push @numbersArray, $value+0; # put the value in the array casting to int
    }
    if($skipHeader>0){
      my @results=GCD_LCM(@numbersArray);
      print "(".$results[1]." ".$results[0].") ";
    }
    $skipHeader++;
}

sub subGCD{#implementing the GCD algorithm with substraction
  my @numbers;
  my $max=max(@_);
  my $min=min(@_);
  push @numbers, $max-$min;
  push @numbers, $min;
  if ($max-$min==0){
  return $min;
  }
  elsif ($max-$min==1){
  return 1;
  }
  else{
    subGCD(@numbers);
  }
}

sub LCM{ #implementing the LCM algorithm
  my ($a,$b)=@_;
  return $a * $b/subGCD(@_);
}

sub GCD_LCM{ #return GCD and LCM in array
  my @result;
  my ($a,$b)=@_;
  my $gcd=subGCD(@_);
  push @result , $a * $b/$gcd;
  push @result , $gcd;
  return @result;
}

#$ perl joregems.pl
#Deep recursion on subroutine "main::subGCD" at joregems.pl line 40, <FH> line5.
#Deep recursion on subroutine "main::subGCD" at joregems.pl line 40, <FH> line6.
#Deep recursion on subroutine "main::subGCD" at joregems.pl line 40, <FH>line12.
#(30 164250) (3 13485) (80 28160) (2 2346) (22 7788) (1 45) (100 77500)
#++ (1 12920) (39 30537) (3 26163) (1 20844) (2 14) (1 4730) (79 211641) (1 10)
#++ (30 48180) (160 184800) (2 40)
