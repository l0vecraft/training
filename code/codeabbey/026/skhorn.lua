-- [[
-- $ luacheck skhorn.lua
-- Checking skhorn.lua             OK
-- Total: 0 warnings / 0 errors in 1 file
-- ]]

-- luacheck: no unused args

-- Check if file exists
local function file_exist(file)
  local f = io.open(file, "rb")
  if f then f:close() end
  return f ~= nil
end

-- Get line by line of the file
local function lines_from(file)
  if not file_exist(file) then return {} end
  local lines = {}
  for line in io.lines(file) do
    lines[#lines + 1] = line
  end
  return lines
end

-- Greatest common divisor fn
local function gcd(a, b)

    local flag = false
    while not flag do
      if a > b then a = a - b end

      if b > a then b = b - a end

      if a == b then flag = true end
    end

    return a

end

-- Least common divisor fn
local function lcm(a, b, gcd_value)

  return (a * b) / gcd_value
end


local file = 'DATA.lst'
local lines = lines_from(file)
local output = {}
for k,v in pairs(lines) do
  if string.len(v) ~= 2 then

    local line_array = {}
    local index = 1
    for item in string.gmatch(v, "%S+") do
      line_array[index] = item
      index = index + 1
    end

    local a = tonumber(line_array[1])
    local b = tonumber(line_array[2])

    local gcd_value = gcd(a, b)
    local lcm_value = math.floor(lcm(a, b, gcd_value))

    local fmt_output = string.format("(%s %s)", gcd_value, lcm_value)
    table.insert(output, fmt_output)
  end

end

print(table.concat(output, " "))
-- [[
-- $ lua5.3 skhorn.lua 
-- (30 164250) (3 13485) (80 28160) (2 2346) (22 7788) (1 45) (100 77500) (1 12920) (39 30537) (3 26163) (1 20844) (2 14) (1 4730) (79 211641) (1 10) (30 48180) (160 184800) (2 40)
-- ]]
