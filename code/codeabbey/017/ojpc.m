inp=[355752 783270 22 70 20068 5 4106 100 3 6 3 4 973 80 883834512 5457 988 407 48 9 12307 482 53 2 11829028];
seed=113;
result=0;
limit=10000007;
for i=1:size(inp,2)
  result=inp(1,i)+result;
  result=result*seed;
  if result>limit
    result=mod(result,limit);
  end
endfor
disp(result)
