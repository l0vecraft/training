###
$ coffeelint --reporter jslint danmur97.coffee #linting
$ coffee -c danmur97.coffee #compilation
###

main = (x)->
  data = x[2]
  data = data.split "\r\n"
  p_words = process_words(x[3])
  counted_words = p_words[1]
  words = p_words[0]
  anagrams_count = []
  for i in [0...data[0]]
    anagrams_count[i] = 0
    for j in [0...counted_words.length]
      console.log("is anagram "+data[i+1]+" and "+words[j])
      console.log(is_anagram(count_letters(data[i+1]),counted_words[j]) &&
      data[i+1] != words[j] && data[i+1].length == words[j].length)
      if(data[i+1].length == words[j].length && data[i+1] != words[j] &&
      is_anagram(count_letters(data[i+1]),counted_words[j]))
        anagrams_count[i]++
  console.clear()
  console.log(to_string(anagrams_count))
  0
to_string = (x)->
  str = ""
  for i in [0...x.length]
    str = str + x[i] + " "
  str

is_anagram = (counted_word,counted_word2)->
  for letter,v of counted_word
    if(counted_word[letter] != counted_word2[letter])
      return false
  return true

process_words = (file)->
  fs = require 'fs'
  dictionary = fs.readFileSync file, 'utf8'
  words = dictionary.split "\r\n"
  counted_words = []
  for w in words
    if(w != "")
      counted_words.push count_letters(w)
  [words, counted_words]

count_letters = (word)->
  count={}
  for i in [0...word.length]
    if(!count[word[i]])
      count[word[i]]=1
    else
      count[word[i]]++
  count

main(process.argv)

###
$ coffee danmur97.coffee "$(< DATA.lst)" "words.lst"
4 3 3 5 3 3 3
###
