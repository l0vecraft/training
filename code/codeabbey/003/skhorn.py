#!/usr/bin/env python3
"""
Problem #3 Sums In Loop
"""
class SumsInLoop:
    """
    Sum each pair of a given set of pair values
    """
    values = []
    lines = []
    sums = 0
    while True:
        line = input()
        if line:
            if len(line) < 3:
                pass
            else:
                values = line.split()
                sums = int(values[0]) + int(values[1])
                lines.append(str(sums))
        else:
            break
        #print("line:%s" % line)
    text = ' '.join(lines)
    print(text)

SumsInLoop()
