--[[

dacevedo@ubundows:~training/challenges/codeabbey/050> (dacevedo)> lua dacevedo.lua

--]]


function main()
  phrases = {}
  n = io.read()

  for i=1, n do
    input = io.read()
    -- Remove spaces and punctuations using a 'regex'-like pattern
    input = string.gsub(input, "[ . , -- / # ! ¡ $ % ^ & * ' + ; : ¿ ? { } = _ ´ ` ~ ( ) ]", "")    
    
    -- Store as a lowercase string (avoid case sensitive comparison)
    phrases[i] = string.lower(input)
  end
  for i=1, n do
    io.write(isPalindrome(phrases[i]) .. " ")
  end
end

function isPalindrome(phrase)
  if string.reverse(phrase) == phrase then
    return "Y"
  else
    return "N"
  end
end

main()

--[[

input:
17
E Dugy Jn xhy-k-X, Kyhxnjygu-De
Ejkumm-Qqewdxjiijxdweq qmmukj, E
Omvbq, g-Ooxecyt, l Ltyc-Exo ogqbvm O
Yytni tun Errjjcto, Uilfyr yy ryf-Liuotc, Jj, rr, enut, intyy
DdEezauflja-aoaa-jlfua Ze e-d
Rjmqlvq fboqvgiqloiy-Ii-Aiiyiolq-igvqobfqvlqmj R
Yyyetntyvdjtvsuqejel-Rl, ypylrlejequs-Vtjdvytnteuyy
Tra, Ibybuk yetoai-eiaoteyku, bybiart
Udgylbemquraiydnwucoijgjiocuwndyia Ruqm, eblygdu
U Eig-en yo uy, n eg ie, u
Alayy, r y Ezbhmcieeiyua-au, yiee, i cm hbzey-ry-yala
Baehbpsgiayiweqhpiua P-Ffpauiph, Qewiyaig SiP-Bhea, b
Ifc-Coycg-iuifieoeifiu igcyoccf, i
Uuuannquclj-h-Ucp-Adpipdapcu, Hj lcu qnnauuu
Pvxeiozo, Mk wwekyayke, w, Wkmozoi-exv P
Ozoowltccgaedevqdexnldjccjdl Nxedqvedeagcctlwo, Ozo
Tuy-wbyyfcwe, Odtuoyuttgt tuyoktdo ewcf-yybwy, ut

output:
Y Y Y Y N Y N Y Y N Y N Y Y Y Y N

--]]
