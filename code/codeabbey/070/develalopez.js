/*
Problem 72: Funny Words Generator
With JavaScript - Node.js
Linting: eslint develalopez.js

--------------------------------------------------------------------
✖ 0 problem (0 error, 0 warnings)
*/

const CONSONANTS = 'bcdfghjklmnprstvwxz';
const VOWELS = 'aeiou';
const MULTIPLIER = 445;
const ADDER = 700001;
const MODULO = 2097152;
const CON_FILTER = 19;
const VOW_FILTER = 5;
const LENGTH = 6;
const WORDS_TO_GENERATE = 900000;

let seed = 0;

function congruentialGenerator(length, previous) {
  seed = ((MULTIPLIER * seed) + ADDER) % MODULO;
  previous.push(seed);
  return length === 1 ? previous : congruentialGenerator(length - 1, previous);
}

function charParser(numbers) {
  const chars = numbers.map((gen, index) => {
    if (index % 2 === 0) {
      return CONSONANTS.charAt(gen % CON_FILTER);
    }
    return VOWELS.charAt(gen % VOW_FILTER);
  });
  return chars.join('');
}

function generateWords() {
  const words = [];
  for (let i = 0; i < WORDS_TO_GENERATE; i++) {
    const word = charParser(congruentialGenerator(LENGTH, []));
    words.push(word);
  }
  return words;
}

function mostFrequentWord(wordsArray) {
  const counts = {};
  let compare = 0;
  let mostFrequent = '';
  for (let i = 0; i < WORDS_TO_GENERATE; i++) {
    const word = wordsArray[i];
    if (word in counts === false) {
      counts[word] = 1;
    } else {
      counts[word] += 1;
    }
    if (counts[word] > compare) {
      compare = counts[word];
      mostFrequent = wordsArray[i];
    }
  }
  return mostFrequent;
}

function dataProcess(erro, contents) {
  if (erro) {
    return erro;
  }
  const dataLines = contents.split('\n');
  const [ firstLine ] = dataLines;
  [ seed ] = firstLine.split(' ').map(Number);
  const generatedWords = generateWords();
  const output = process.stdout.write(`${ mostFrequentWord(generatedWords) }`);
  return output;
}

const filesRe = require('fs');
function main() {
  return filesRe.readFile('DATA.lst', 'utf-8', (erro, contents) =>
    dataProcess(erro, contents));
}

main();

/*
$ node develalopez.js
wipemu
*/
