clc
clear all
prompt = 'Digit the cards between []: ';
cards = [];
cards = input(prompt);

suits = {'Clubs', 'Spades', 'Diamonds', 'Hearts'};
ranks = {'2', '3', '4', '5', '6', '7', '8', '9', '10', 'Jack', 'Queen', 'King', 'Ace'};

n = size(cards,2);
res = {};
of = '-of-';
for i = 1 :n
    temp = [];
    card = cards(i);
    suits_v = floor(card/13) + 1;
    ranks_v = mod(card,13) + 1;
    suit = suits(suits_v);
    rank = ranks(ranks_v);
    temp = strcat(rank,of,suit);
    res(i) = temp;
end
disp('The values of the cards are');
disp(cards);
disp('The ranks and Suits that correspond to the mention before cards:');
disp(res);

