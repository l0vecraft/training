; $ lein eastwood
;== Eastwood 0.3.5 Clojure 1.10.0 JVM 11.0.4 ==
;Directories scanned for source files:
;
;src test
;== Linting adrianfcn.core ==
;== Linting adrianfcn.core-test ==
;== Linting done in 1151 ms ==
;== Warnings: 0 (not including reflection warnings)  Exceptions thrown: 0
(ns adrianfcn.core
  (:gen-class)
  (:require [clojure.math.numeric-tower :as math])
  (:require [clojure.string :as str])
  (:require [clojure.java.io :as input]))

(def filePath "DATA.lst")

(defn solution [X1 Y1 X2 Y2 X3 Y3]
  (let [n (- (+ (* X2 Y1 ) (+ (* X3 Y2) (* X1 Y3)))
             (+ (* X1 Y2 ) (+ (* X2 Y3) (* X3 Y1)))
          )
       ]
    (if (= (rem n 2) 0)
      (print (math/abs(/ n 2)))
    )
    (if (not= (rem n 2) 0)
      (format "%.1f" (math/abs (float (/ n 2))))
    )
  )
)

(defn s2c [l]
  (into [] (map #(Integer/parseInt %) (str/split l #" ")))
)

(defn read_file
  ([file]
    (with-open [rdr (input/reader file)]
      (doseq [line (line-seq rdr)]
        (let [c  (s2c line)
              s  (count c)
              X1  (get c 0)
              Y1  (get c 1)
              X2  (get c 2)
              Y2  (get c 3)
              X3  (get c 4)
              Y3  (get c 5)]
          (if (= s 6)
            (print (str (solution X1 Y1 X2 Y2 X3 Y3) " "))
          )
        )
      )
    )
    (println)
  )
)

(defn -main [& args]
  (read_file "DATA.lst")
)

(-main)
; $ clojure adrianfcn.clj
; 8008545,5 9597811 11915667 11450171 15066084 2427172,5 3265797 31022920
; 492795 5095621 4526802 8112862,5 1371713 1036641 7015461,5 20091288,0

