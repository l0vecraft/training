"""
  Feature: Solving the challenge #179 Look and say binary
  With Python v3
  From http://www.codeabbey.com/index/task_view/look-and-say-binary


 Linting:   pylint jenniferagve.py
    --------------------------------------------------------------------
    Your code has been rated at 10.00/10 (previous run: 10.00/10, +0.00)

"""

DATA = open('DATA.lst', "r")
DATA = DATA.readline()
COUNTER = 0
DATAMANAGEMENT = ""
FINALDATA = ""
LENGTH = len(DATA)
PARENT = 0
POSITION = 0


for INDEX in DATA:
    if INDEX == "1":
        PARENT += 1

while FINALDATA != "10":

    if POSITION != -1:
        if DATA[0] == "1":
            POSITION = DATA.find("0")
        elif DATA[0] == "0":
            POSITION = DATA.find("1")

        if POSITION != -1:

            DATAMANAGEMENT = DATAMANAGEMENT + bin(POSITION)[2:]
            DATA = DATA[POSITION:len(DATA)]
            LENGTH = LENGTH - POSITION

        else:
            POSITION = LENGTH
            DATAMANAGEMENT = DATAMANAGEMENT + bin(POSITION)[2:]
            COUNTER += 1
            DATA = DATAMANAGEMENT
            FINALDATA = DATAMANAGEMENT
            LENGTH = len(DATA)
            POSITION = 0
            DATAMANAGEMENT = ""

ANSWER = str(COUNTER) + " " + str(2 ** (PARENT - 1))
print ANSWER

# pylint: disable=pointless-string-statement

''' python jenniferagve.py
    input: 1110100011000011001001001111011110000100111000101110111100
    output: 25 1180591620717411303424 '''
