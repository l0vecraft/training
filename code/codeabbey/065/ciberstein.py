#!/usr/bin/env python
"""
Codeabbey challenge 65
"""
# > pylint ciberstein.py
# Your code has been rated at 10.00/10 (previous run: 10.00/10, +0.00)

import os.path as path
from numpy import full

if path.exists('./DATA.lst'):
    FILE = open('DATA.lst', 'r')
    LINES = FILE.read().split('\n')
    ROADS = int(LINES[0])
    CASE = int(LINES[ROADS+1])
    ARRAY = []

    for i in range(1, ROADS + 1):
        states = LINES[i].split(' - ')

        for k in range(2):
            if states[k] not in ARRAY:
                ARRAY.append(states[k])

    N = len(ARRAY)
    G = full((N, N), 10000000)

    for i in range(1, ROADS + 1):
        states = LINES[i].split(' - ')
        x = ARRAY.index(states[0])
        y = ARRAY.index(states[1])
        G[(x, y)] = 1
        G[(y, x)] = 1

    for k in range(N):
        for i in range(N):
            for j in range(N):
                G[(i, j)] = min(G[(i, j)], G[(i, k)] + G[(k, j)])

    for i in range(ROADS + 2, ROADS + CASE + 2):
        states = LINES[i].split(' - ')
        x = ARRAY.index(states[0])
        y = ARRAY.index(states[1])
        print int(G[(x, y)])
else:
    print 'Error DATA.lst not found'

# > python ciberstein.py
# 2 3 5 2 3 2 3 2 2 2 2 2 4 3 2 4 3
