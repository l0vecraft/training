<?php
/**
 *
 * $ phpcs dianaosorio97.php #linting
 * $ php dianaosorio97.php  #Compilation
 *
*/

if (file_exists('DATA.lst')) {
  $file = fopen("DATA.lst", "r");
  $numbers = explode(' ', fgets($file, 128));
  $nodes = $numbers[0];
  $edges = $numbers[1];
  $graph = [];
  $stack = new SplStack();
  $seen = [];
  $stack->push(0);
  $tuple = [];
  $neighbors = [];
  $answer = array(0=>-1);
  for ($i=0; $i < $edges; $i++) {
    $vertex = array_map('intval', explode(' ', fgets($file, 128)));
    array_push($graph, $vertex);
  }
  while ($stack->count() > 0) {
    $nodeCurrent = $stack->pop();
    if (!in_array($nodeCurrent, $seen)) {
      array_push($seen, $nodeCurrent);
      foreach ($graph as $child) {
        if (in_array($nodeCurrent, $child)) {
          $tuple[] = $child;
        }
      }
      foreach ($tuple as $k) {
        $index = array_search($nodeCurrent, $k);
        unset($k[$index]);
        array_push($neighbors, $k);
      }
      $neighbors = array_merge(...$neighbors);
      rsort($neighbors);
      foreach ($neighbors as $k) {
        $stack->push($k);
        if (in_array($k, $seen)==0) {
          $answer[$k] = $nodeCurrent;
        }
      }
      $tuple = [];
      $neighbors = [];
    }
  }
    for ($i=0; $i < count($answer); $i++) {
      echo($answer[$i])." ";
    }
  echo "\n";
} else {
    echo "Fail";
}

/*
php dianaosorio97.php
output:
-1 7 19 22 10 2 5 21 35 6 11 0 27 1 13 8 38 3 34 4 15
16 28 7 26 18 17 20 44 25 24 42 26 18 12 14 30 30 9
17 22 24 20 22 29
*/
?>
