/*
Linting with CppCheck assuming the #include files are on the same
folder as kedavamaru.cpp
> cppcheck --enable=all --inconclusive --std=c++14 kedavamaru.cpp
Checking kedavamaru.cpp ...

Compiling and linking using the "Developer Command Prompt for VS 2017"
> cl /EHsc kedavamaru.cpp

Microsoft (R) C/C++ Optimizing Compiler Version 19.15.26726 for x86
Copyright (C) Microsoft Corporation.  All rights reserved.

kedavamaru.cpp

Microsoft (R) Incremental Linker Version 14.15.26726.0
Copyright (C) Microsoft Corporation.  All rights reserved.

/out:kedavamaru.exe
kedavamaru.obj
*/

#include <iostream>
#include <fstream>

using namespace std;
using uint = unsigned int;

class node {
public:
  node *l = nullptr;
  uint val = 0xFFFF;
  node *r = nullptr;
};

void insert(uint value, node *n) {
  if (n->val == 0xFFFF) {
    n->val = value;
  }
  else {
    if (value > n->val) {
      if (n->r == nullptr) n->r = new node;
      insert(value, n->r);
    }
    if (value < n->val) {
      if (n->l == nullptr) n->l = new node;
      insert(value, n->l);
    }
  }
}

void traverse(node *n) {
  cout << "(";
  if (n->l != nullptr) traverse(n->l);
  else cout << "-";
  cout << ",";
  cout << n->val;
  cout << ",";
  if (n->r != nullptr) traverse(n->r);
  else cout << "-";
  cout << ")";
}

int main() {
  ifstream file("DATA.lst");
  if (!file) return 1;

  uint n; file >> n;

  node *root = new node;

  for (uint i = 0, value; i < n; ++i) {
    file >> value;
    insert(value, root);
  }

  traverse(root);
}
/*
Running using the "Windows Command Prompt", assumming "DATA.lst" is
on the same folder as kedavamaru.exe

> kedavamaru.exe
(((((((-,1,-),2,(-,3,-)),4,((-,5,-),6,(-,7,-))),8,-),9,(-,10,(-,11,-)
)),12,((-,13,-),14,((-,15,(-,16,((-,17,-),18,(((-,19,((-,20,-),21,-))
,22,-),23,-)))),24,-))),25,-)
*/
