let n = Scanf.scanf "%d\n"(fun n ->  n);;

type 'a tree = Empty
             | Node of 'a * 'a tree * 'a tree;;

let rec insert x = function
  | Empty -> Node(x, Empty, Empty);
  | Node(k, left, right) ->
    if int_of_string(x) < int_of_string(k) then Node(k, insert x left, right)
    else Node(k, left, insert x right)
    ;;

let rec inorder t s= match t with
    |  Empty -> s := !s ^ "-"
    |  Node (k,l,r) ->  s := !s ^ "(";
                        inorder l s;
                        s := !s ^ ",";
                        s := !s ^ k;
                        s := !s ^ ",";
                        inorder r s;
                        s := !s ^ ")"

let values = read_line () ;;
let strlist = Str.split (Str.regexp "[ ]+") values ;;
let tree = ref (Node((List.hd strlist), Empty, Empty));;

for i = 1 to (List.length strlist) -1 do
    let v = List.nth strlist i in
    tree := insert v !tree;
done;;

let s = ref "";;
let () = inorder !tree s;;
print_string !s;;
