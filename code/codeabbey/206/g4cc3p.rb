# root@g4cc3p:~/RubymineProjects/g4cc3p# rubocop -l g4cc3p.rb
# Inspecting 1 file
# .
#
# 1 file inspected, no offenses detected

# frozen_string_literal: true

BASE32_CHARACTERS = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ234567'.freeze

def get_base32_char(index)
  BASE32_CHARACTERS[index]
end

def get_base32_char_pos(character)
  BASE32_CHARACTERS.index(character)
end

def add_padding(raw_string)
  needed_padding = 5 - raw_string.length % 5
  raw_string.ljust(needed_padding + raw_string.length, needed_padding.to_s)
end

def remove_padding(padded_string)
  pad_used = padded_string[-1].to_i(10)
  padded_string[0, padded_string.length - pad_used]
end

def base32_encode(str2encode)
  bin_string = ''
  encode_string = ''
  ba_str2encode = add_padding(str2encode).bytes
  ba_str2encode.each { |byte| bin_string += byte.to_s(2).rjust(8, '0') }
  bin_chunks = bin_string.chars.each_slice(5).map(&:join)
  bin_chunks.each { |chunk| encode_string += get_base32_char(chunk.to_i(2)) }
  encode_string
end

def base32_decode(str2decode)
  bin_string = ''
  padded_string = ''
  str2decode.each_char do |char_2decode|
    bin_string += get_base32_char_pos(char_2decode).to_s(2).rjust(5, '0')
  end
  bin_chunks = bin_string.chars.each_slice(8).map(&:join)
  bin_chunks.each { |chunks| padded_string += chunks.to_i(2).chr }
  remove_padding(padded_string)
end

def output_solutions(data)
  test_cases = data.length - 1
  (1..test_cases).each do |test|
    if data[test] =~ /[A-Z]+/
      print base32_decode(data[test]) + "\n"
    else
      print base32_encode(data[test]) + "\n"
    end
  end
end

def read_test_cases
  file = 'DATA.lst'
  data = []
  fp = File.open(file, 'r')
  fp.each_line do |line|
    data.push(line.to_s.strip)
  end
  output_solutions(data)
end

read_test_cases

# root@g4cc3p:~/RubymineProjects/g4cc3p# ruby g4cc3p.rb
# OBQXE4TPOQ2DINBU
# induced rejuvenation
# MVTGMZLSOZSXGY3FNZ2DGMZT
# ..........
# ..........
# dramatic boomerang oversensitive existentially bathes
