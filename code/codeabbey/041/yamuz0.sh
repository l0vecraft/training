#!/usr/bin/env bash
# $ shellcheck yamuz0.sh
# $

# loading triplets from DATA.lst
all_triplets=($(cat "DATA.lst"))
# deleting the information about amount of triplets due it doesn't matter
unset all_triplets[0]
all_triplets=( "${all_triplets[@]}" )
# initializing the array where will be saved the results of the script
results=()

# while there are remaining triplets to analyze keep running
while [ ${#all_triplets[@]} -ne 0 ]; do
  # selecting triplet to analyze.
  triplet_to_analyze=(${all_triplets[@]:0:3})
  # deleting at the whole array of triplets, the triplet to be analyzed
  unset all_triplets[0]
  unset all_triplets[1]
  unset all_triplets[2]
  all_triplets=( "${all_triplets[@]}" )
  # sorting the current analyzed triplet from minimum to maximum
  sorted_triplet=($(echo "${triplet_to_analyze[*]}" | tr " " "\n" | sort -n))
  # saving found medians
  results=("${results[@]}" ${sorted_triplet[1]})
done

# printing medians of triplets
echo "${results[@]}"

# $ ./yamuz0.sh
# 4 1087 22 57 503 80 161 811 620 112 952 52 15 847 13 90 38 36 308 14 971 117
# 250 284 11 929 151 358
