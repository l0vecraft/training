using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
namespace MyNameSpace
{
    class Program{
      static void Main(string[] args){
          int pruebas;
          int[] casos;
          MedianThree mt;
          Console.WriteLine("ingrese el numero de pruebas:");
          pruebas = Int32.Parse(Console.ReadLine());
          mt = new MedianThree();
          mt.Calculate(pruebas);
      }
    }
    class MedianThree
    {
          String cadena;
          int[] arrayNumbers = new int[3];
          int[] casos;
          int[] output;
          int c = 0;
          public void Calculate(int size){
            casos = new int[size];
            output = new int[size];
            for (int i = 0; i < casos.Length; i++)
            {
                  Console.WriteLine("Prueba "+ (i+1) +", ingrese tres numeros separados por un espacio:");
                  cadena = Console.ReadLine();
                  arrayNumbers = cadena.Split(' ').Select(str => int.Parse(str)).ToArray();
                  for (int j = 0; j < arrayNumbers.Length; j++)
                  {
                    if( (arrayNumbers.Min() < arrayNumbers[j]) && (arrayNumbers[j] < arrayNumbers.Max()) ){
                      output[c] = arrayNumbers[j];
                      c++;
                    }
                  }
            }
            foreach(int i in output)
            {
              Console.Write("{0} ", i);
            }
          }
    }
}
