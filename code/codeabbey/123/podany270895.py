"""
$ pylint podany270895.py
No config file found, using default configuration

--------------------------------------------------------------------
Your code has been rated at 10.00/10 (previous run: 10.00/10, +0.00)

[dsalazar@localhost 123]$ pylint podany270895.py
No config file found, using default configuration

--------------------------------------------------------------------
Your code has been rated at 10.00/10 (previous run: 10.00/10, +0.00)
"""

import numpy as np


def read_input_file(path):
    """
    Parses input file and collects item weights-values,
    items number and max size
    """
    input_file = open(path).read().split()
    input_file = [int(x) for x in input_file]
    items_number = input_file[0]
    maximum_size = input_file[1]
    values = []
    weights = []
    for i in xrange(2, items_number*2+1, 2):
        item_weight = input_file[i]
        item_value = input_file[i+1]
        values.append(item_value)
        weights.append(item_weight)
    return items_number, maximum_size, weights, values


# Finds the matrix containing the solution to the problem
def knapsack(items_number, maximum_size, weights, values):
    """
    Finds the matrix containing the solution to the problem
    """
    answer = np.zeros((items_number+1, maximum_size+1))
    for i in xrange(items_number+1):
        for j in xrange(maximum_size+1):
            # if I have 0 items or 0 maximum_size, nothing can be stored
            if i == 0 or j == 0:
                answer[i, j] = 0
            # The weight of the item evaluating is greater than the max_size
            elif weights[i-1] > j:
                answer[i, j] = answer[i-1, j]
            # Deciding if the new item is better than the current one
            else:
                answer[i, j] = max(
                    answer[i-1, j], answer[i-1, j-weights[i-1]] + values[i-1])
    # The last row and column of the matrix contain the highest possible value
    return answer


def main():
    """
    Main function that reads the input file, creates the answer matrix
    and prints the result
    """
    items_number, maximum_size, weights, values = read_input_file("DATA.lst")
    answer = knapsack(items_number, maximum_size, weights, values)
    print(answer[-1, -1])


main()

# pylint: disable=pointless-string-statement

"""
$ python podany270895.py
output:
4289.0
"""
