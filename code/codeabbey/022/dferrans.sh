#!/bin/bash
# $ shellcheck dferrans #linting
#
function twoprinters(){
 array_data=($1)
 first_printer="${array_data[0]}"
 second_printer="${array_data[1]}"
 total_pages="${array_data[2]}"
 maxv="$first_printer"
 minv="$second_printer"

 if [[ $second_printer -gt  $first_printer ]]
 then
    maxv="$second_printer"
    minv="$first_printer"
 fi

 let sum_printers="$first_printer + $second_printer"
 maxt="$(("$total_pages" * "$minv" ))"
 maxt_dec=$(bc <<< "scale=5; $maxt/$sum_printers")
cl_mx=$( echo "$maxt_dec" | awk '{print ($0-int($0)<0.001)?int($0):int($0)+1}')
 final_max="$(("$cl_mx" * "$maxv"))"
 mint="$(( "$total_pages" * "$maxv" ))"
 mnt_dec=$(bc <<< "scale=5; $mint/$sum_printers")
cl_min=$( echo "$mnt_dec" | awk '{print ($0-int($0)<0.001)?int($0):int($0)+1}')
 final_min="$(("$cl_min" * "$minv"))"
 result="$final_min"

if [[ $final_min -gt  $final_max ]]
then
    result="$final_max"
fi

echo "$result"
}

count=0

while read -r line || [[ -n "$line" ]];
do
    if [[ $count -gt  0 ]]
    then
        values=$(echo "$line" | tr " " "\n")
        result_array=$(twoprinters "$values")
        echo "$result_array"
    fi

    let "count +=1"

done < "$1"

# ./dferrans.sh DATA.lst
# 318244719 13158054 278833660 132435810 51272717 46729010 8760609 107074208
# 309055713 33295378 322580664 359769725 332892224 162592318 175123025
