#!/usr/bin/env python2.7
"""
$ pylint jicardona.py
No config file found, using default configuration

------------------------------------
Your code has been rated at 10.00/10
"""

GRID = []

with open('DATA.lst') as data:
    for line in data:
        GRID.append(line.split())

MOVES = GRID.pop(-1)

SNAKE = [(0, 0), (1, 0), (2, 0)]


def make_move(step, snake_dir, total):
    """Move the SNAKE through the GRID"""

    if step.isdigit() is True and step != '0':
        new_head = None
        if snake_dir == 'R':
            new_head = (SNAKE[-1][0] + 1, SNAKE[-1][1])
            if SNAKE[-1][0] + 1 > 20:
                new_head = (0, SNAKE[-1][1])
        if snake_dir == 'D':
            new_head = (SNAKE[-1][0], SNAKE[-1][1] + 1)
            if SNAKE[-1][1] + 1 > 12:
                new_head = (SNAKE[-1][0], 0)
        if snake_dir == 'L':
            new_head = (SNAKE[-1][0] - 1, SNAKE[-1][1])
            if SNAKE[-1][0] - 1 < 0:
                new_head = (20, SNAKE[-1][1])
        if snake_dir == 'U':
            new_head = (SNAKE[-1][0], SNAKE[-1][1] - 1)
            if SNAKE[-1][1] - 1 < 0:
                new_head = (SNAKE[-1][0], 12)
        SNAKE.append(new_head)
        step = str(int(step) - 1)
        total += 1
        if GRID[SNAKE[-1][1]][SNAKE[-1][0]] == '-':
            tail = SNAKE.pop(0)
            GRID[tail[1]][tail[0]] = '-'
        if GRID[SNAKE[-1][1]][SNAKE[-1][0]] == 'X':
            return ' '.join([str(SNAKE[-1][0]), str(SNAKE[-1][1]), str(total)])
        GRID[SNAKE[-1][1]][SNAKE[-1][0]] = 'X'
        return make_move(step, snake_dir, total)
    snake_dir, step = (step, '0') if step.isalpha() else (snake_dir, step)
    more = len(MOVES) > 0 and step == '0'
    if not more:
        return None
    step = MOVES.pop(0)
    return make_move(step, snake_dir, total)


print make_move('0', 'R', 0)

# $ python jicardona.py
# 6 9 45
