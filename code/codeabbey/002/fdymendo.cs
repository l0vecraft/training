using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Fluid
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Digite la cantidad de número a digitar y sumar:");
            String temp1 = Console.ReadLine();
            int repeticiones = int.Parse(temp1);
            int total = 0;
            for (int i = 0; i < repeticiones; i++)
            {
                Console.WriteLine("Por favor digite el número " + i + 1 + ": ");
                total += int.Parse(Console.ReadLine());

            }
            Console.WriteLine("El resultado es: " + total);
            Console.ReadKey();
        }
    }
}
