#!/usr/bin/env jsc
/*
$ eslint jicardona.js
*/

/* global print */

const [ input ] = arguments;
const testCases = input.split(/[\s\n]/);
const total = testCases.shift();

const answser = [];

const length = 8;
const lower = 2;
const upper = 6;

for (let testCase = 0; testCase < total; testCase++) {
  let initialValue = parseInt(testCases[testCase], 10);
  const values = [];
  do {
    values.push(initialValue);
    initialValue *= initialValue;
    let truncVal = initialValue.toString();
    const zeroes = length - truncVal.length;
    truncVal = ('0'.repeat(zeroes)).concat(truncVal);
    initialValue = parseInt(truncVal.slice(lower, upper), 10);
  } while (!values.includes(initialValue));
  answser.push(values.length);
}

print(answser.join(' '));

/*
$ jsc jicardona.js -- "`cat DATA.lst`"
107 105 104 100 101 110 97 103 108 103 101 106
*/
