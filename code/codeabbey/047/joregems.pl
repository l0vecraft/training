#$perl -Mstrict -Mdiagnostics -cw joregems.pl
#joregems.pl syntax OK
#!/usr/bin/perl

use List::Util qw[min max];
use List::Util qw/sum/;
use warnings;
use strict;

my $file="DATA.lst"; ##name where is allocated
#the file with the ciphered phrases
begin($file);

sub uncipher{# function to un-cipher the phrases
  my ($phrase,$shift_value)=@_;#assignation to variables
    my @characters_phrase = split("", $phrase);#converting an string to
    #an array with all character from original string.
  foreach my $letter(@characters_phrase){#go through the array
  #with the string characters
    my $num_letter  = ord($letter);#convert the letters to ascii number
    my $number_unciphered=get_value_uncipher($num_letter,$shift_value);
    #calling the function to get the un-ciphered value in ascii of the
    #ciphered letter
    my $letter_unciphered = chr($number_unciphered);#transform a number to
    #an ascii character
    print $letter_unciphered;
  }
}

sub get_value_uncipher{#function to transform an ciphered ascii letter value
#in to an un-ciphered ascii letter value
  my ($number_ciphered,$shift_value)=@_;
  my $number_unciphered;

  if($number_ciphered >= 65 and $number_ciphered <= 90){
  #value of A in ascii is 65 and value of Z in ascii is 90
  #then we are only operating with letters, uppercase letters
    if($number_ciphered-$shift_value<65){#if subtract the shift value to the
    #ciphered letter is less than the value of A in ascii
      $number_unciphered = (26-$shift_value)+$number_ciphered;#sums 26, because
      #we count A as 0 and we subtract the shift value
    }
    else{
      $number_unciphered = $number_ciphered-$shift_value;
      #if subtract shift value to the ciphered number is greater than the value
      #of ascii A value, then there is any problem and I can transform directly
      #the number value to another uppercase letter
    }
    return $number_unciphered;
  }
  elsif($number_ciphered==10){#if there is any line feed
  #then let's transform it in to an space
    return 32;
  }
  else{
    return $number_ciphered;#here we process the characters others than
  #uppercase letters and line feed
  }

}

sub begin{
  my $data = $_[0]; #the variable associated to the file
  #that contains the ciphered phrases
  my $skipHeader=0; #variable for skip the first line of the file
  open(FH, '<', $data) or die $!; #open the file with the ciphered phrases
  my $shift_val=6;
  while(<FH>){ #go through the file line by line
    if ($skipHeader==0){
      my @line = split(' ', $_); #splitting the line of the file
      #to get second parameter
      $shift_val=$line[-1];
      $skipHeader++;
    }
    else{
      print uncipher($_,$shift_val); #un-cipher the line
    }
  }
  print "\n"
}

#$perl joregems.pl
#CALLED IT THE RISING SUN FOUR SCORE AND SEVEN YEARS AGO THE ONCE AND FUTURE
# KING. A DAY AT THE RACES. MET A WOMAN AT THE WELL. THE DEAD BURY THEIR OWN
# DEAD THAT ALL MEN ARE CREATED EQUAL. AS EASY AS LYING. AND SO YOU TOO BRUT
#US. THE SECRET OF HEATHER ALE WHO WANTS TO LIVE FOREVER.
