--[[

dacevedo@ubundows:~training/challenges/codeabbey/047> (dacevedo)> lua dacevedo.lua

--]]

_ABC = {}

function main()
  
  i = 0
  for letter=65, 90 do
    _ABC[i] = string.char(letter)
    i = i + 1
  end
  
  input = {}
  inputSplit = string.gmatch(io.read(),"%S+")
  io.flush()
  
  i = 0
  for item in inputSplit do
    input[i] = item
    i = i + 1
  end
  
  totalPhrases = input[0]
  k = input[1]
  
  phrases = {}
  for i=1, totalPhrases do
    phrases[i] = io.read()
  end
  io.flush()
  
  caesarDecrypt(phrases, k)
end

function caesarDecrypt(phrases, k)
  out = ""
  
  for key,value in pairs(phrases) do
    for word in string.gmatch(phrases[key], "%S+") do
      for letter in string.gmatch(word, ".") do
        out = out .. getCaesarLetter(letter, k)
      end
      out = out .. " "
    end
  end
  
  print (out)
end

function getCaesarLetter(letter, k)
  i = 0
  found = false
  
  while i < #_ABC + 1 and not found do
    if _ABC[i] == letter then
      found = true
      for j = 1, k do
        i = i - 1
        if i == -1 then
          i = #_ABC
        end
      end
    else
      i = i + 1
    end
  end

  if found then
    return _ABC[i]
  else
    return letter
  end
end

main()

--[[

input:
6 22
PDWP WHH IAJ WNA YNAWPAZ AMQWH PK QO EJ KHZAJ OPKNEAO PDA ZAWZ XQNU PDAEN KSJ ZAWZ.
EJ WJYEAJP LANOEW PDANA SWO W GEJC W ZWU WP PDA NWYAO.
CERA UKQN NKKGO XQP JKP ZEHWNWI CNAAJBEAHZO WNA CKJA JKS.
W JECDP WP PDA KLANW WJZ OK UKQ PKK XNQPQO IAP W SKIWJ WP PDA SAHH.
WJZ BKNCERA QO KQN ZAXPO BKQN OYKNA WJZ OARAJ UAWNO WCK WO AWOU WO HUEJC.
HAP DEI PDNKS PDA BENOP OPKJA PDA OAYNAP KB DAWPDAN WHA WNA SKJZANO IWJU PKHZ.

output:
THAT ALL MEN ARE CREATED EQUAL TO US IN OLDEN STORIES THE DEAD BURY THEIR OWN DEAD. IN ANCIENT PERSIA THERE WAS A KING A DAY AT THE RACES. GIVE YOUR ROOKS BUT NOT DIL
ARAM GREENFIELDS ARE GONE NOW. A NIGHT AT THE OPERA AND SO YOU TOO BRUTUS MET A WOMAN AT THE WELL. AND FORGIVE US OUR DEBTS FOUR SCORE AND SEVEN YEARS AGO AS EASY AS
LYING. LET HIM THROW THE FIRST STONE THE SECRET OF HEATHER ALE ARE WONDERS MANY TOLD.

--]]
