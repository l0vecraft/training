/**
 * tsc ./src/wgiraldom1.tsc
 * tslint -p ./
 */

import * as fs from "fs";

function f(s: string, a: number[]): string {
  /**
   * This is how the cookie crumbles:
   * Having a string s, we need to append to s the appropiate characters
   * mandated by the next two bytes in a.
   */
  if (a.length === 0) {
    return s;
  }
  const a0: number = a[0];
  const a1: number = a[1];
  const slicedArr: number[] = a.slice(2, a.length);
  if (a0 === 0) {
    return f(s + String.fromCodePoint(a[1]), slicedArr);
  }
  const sp: string = g(s, a0, a1);

  return f(sp, slicedArr);
}

function g(s: string, a0: number, a1: number): string {
  /**
   * Given a string s and a pair of bytes a0, a1,
   * calculate the offset and length o, l
   * and append l bytes to s, starting from the o-th
   * character to the right.
   */
  const itemLength: number = (a0 & 0xF0) >> 4;
  const itemOffset: number = (a0 & 0x0F) << 8 | a1;

  return s + s.substr(s.length - itemOffset - 1, itemLength);
}

fs.readFile("DATA.lst", (errB: Error | null, data: Buffer) => {
  if (errB !== null) {
    return errB;
  }
  const a: number[] = data
    .toString()
    .split(" ")
    .map((v: string) => parseInt(v, 16));
  process.stdout.write(f("", a));
});

/*
>node wgiraldom1.js
and enlarging its Boundaries so as to render it at once an example and fit inst
rument for introducing the same absolute rule into these states For taking away
 our Charters abolishing our most valuable Laws and altering fundamentally the
Forms of our Governments For suspending our own Legislatures and declaring them
selves invested with power to legislate for us in all cases whatsoever He has a
bdicated Government here by declaring us out of his Protection and waging War a
gainst us He has plundered our seas ravaged our coasts burnt our towns and dest
royed the lives of our people He is at this time transporting large Armies of f
oreign Mercenaries to compleat the works of death desolation and tyranny alread
y begun with circumstances of Cruelty and Perfidy scarcely paralleled in the mo
st barbarous ages and totally unworthy the Head of a civilized nation He has co
nstrained our fellow Citizens taken Captive on the high Seas to bear Arms again
st their Country to become the executioners of their friends and Brethren or to
 fall themselves by their Hands He has excited domestic insurrections amongst u
s and has endeavoured to bring on the inhabitants of our frontiers the merciles
s Indian Savages whose known rule of warfare is an undistinguished destruction
of all ages sexes and conditions In every stage of these Oppressions We have Pe
titioned for Redress in the most humble terms Our repeated Petitions have been
answered only by repeated injury A Prince whose character is thus marked by eve
ry act which may define a Tyrant is unfit to be the ruler of a free people Nor
have We been wanting in attentions to our British brethren We have warned them
from time to time of attempts by their legislature to extend an unwarrantable j
urisdiction over us We have reminded them of the circumstances of our emigratio
n and settlement here We have appealed to their native justice and magnanimity
and we have conjured them by the ties of our common kindred to disavow these us
urpations which would inevitably interrupt our connections and correspondence T
hey too have been deaf to the voice of justice and of consanguinity We must the
refore acquiesce in the necessity which denounces our Separation and hold them
as we hold the rest of mankind Enemies in War in Peace Friends We therefore the
 Representatives of the united States of America in General Congress Assembled
appealing to the Supreme Judge of the world for the rectitude of our intentions
 do in the Name and by Authority of the good People of these Colonies solemnly
publish and declare That these united Colonies are and of Right ought to be Fre
e and Independent States that they are Absolved from all Allegiance to the Brit
ish Crown and that all political connection between them and the State of Great
 Britain is and ought to be totally dissolved and that as Free and Independent
States they have full Power to levy War conclude Peace contract Alliances estab
lish Commerce and to do all other Acts and Things which Independent States may
of right do And for the support of this Declaration with a firm reliance on the
 protection of divine Providence we mutually pledge to each other our Lives our
 Fortunes and our sacred Honor The unanimous Declaration of the thirteen United
 States of America When in the Course of human events it becomes necessary for
one people to dissolve the political bands which have connected them with anoth
er and to assume among the powers of the earth the separate and equal station t
o which the Laws of Nature and of Natures God entitle them a decent respect to
the opinions of mankind requires that they should declare the causes which impe
l them to the separation We hold these truths to be selfevident that all men ar
e created equal that they are endowed by their Creator with certain unalienable
 Rights that among these are Life Liberty and the pursuit of Happiness That to
secure these rights Governments are instituted among Men deriving their just po
wers from the consent of the governed That whenever any Form of Government beco
mes destructive of these ends it is the Right of the People to alter or to abol
ish it and to institute new Government laying its foundation on such principles
 and organizing its powers in such form as to them shall seem most likely to ef
fect their Safety and Happiness Prudence indeed will dictate that Governments l
ong established should not be changed for light and transient causes and accord
ingly all experience hath shewn that mankind are more disposed to suffer while
evils are sufferable than to right themselves by abolishing the forms to which
they are accustomed But when a long train of abuses and usurpations pursuing in
variably the same Object evinces a design to reduce them under absolute Despoti
sm it is their right it is their duty to throw off such Government and to provi
de new Guards for their future security Such has been the patient sufferance of
 these Colonies and such is now the necessity which constrains them to alter th
eir former Systems of Government The history of the present King of Great Brita
in is a history of repeated injuries and usurpations all having in direct objec
t the establishment of an absolute Tyranny over these States To prove this let
Facts be submitted to a candid world He has refused his Assent to Laws the most
 wholesome and necessary for the public good He has forbidden his Governors to
pass Laws of immediate and pressing importance unless suspended in their operat
ion till his Assent should be obtained and when so suspended he has utterly neg
lected to attend to them He has refused to pass other Laws for the accommodatio
n of large districts of people unless those people would relinquish the right o
f Representation in the Legislature a right inestimable to them and formidable
to tyrants only He has called together legislative bodies at places unusual unc
omfortable and distant from the depository of their Public Records for the sole
 purpose of fatiguing them into compliance with his measures He has dissolved R
epresentative Houses repeatedly for opposing with manly firmness of his invasio
ns on the rights of the people He has refused for a long time after such dissol
utions to cause others to be elected whereby the Legislative Powers incapable o
f Annihilation have returned to the People at large for their exercise the Stat
e remaining in the mean time exposed to all the dangers of invasion from withou
t and convulsions within He has endeavoured to prevent the population of these
States for that purpose obstructing the Laws for Naturalization of Foreigners r
efusing to pass others to encourage their migrations hither and raising the con
ditions of new Appropriations of Lands He has obstructed the Administration of
Justice by refusing his Assent to Laws for establishing Judiciary Powers He has
 made Judges dependent on his Will alone for the tenure of their offices and th
e amount and payment of their salaries He has erected a multitude of New Office
s andsent hither swarms of Officers to harass our people and eat out their subs
tance He has kept among us in times of peace Standing Armies without the Consen
t of our legislatures He has affected to render the Military independent of and
 superior to the Civil Power He has combined with others to subject us to a jur
isdiction foreign to our constitution and unacknowledged by our laws giving his
 Assent to their Acts of pretended Legislation For quartering large bodies of a
rmed troops among us For protecting them by a mock Trial from punishment for an
y Murders which they should commit on the Inhabitants of these States For cutti
ng off our Trade with all parts of the world For imposing Taxes on us without o
ur Consent For depriving us in many cases of the benefit of Trial by Jury For t
ransporting us beyond Seas to be tried for pretended offences For abolishing th
e free System of English Laws in a neighbouring Province establishing therein a
n Arbitrary government
*/
