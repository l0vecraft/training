/*
$ rustfmt santiyepes.rs --write-mode=diff
$
$ rustc santiyepes.rs
$
*/
use std::fs::File;
use std::io::{BufRead, BufReader};

fn main() {
  let file = File::open("DATA.lst").unwrap();

  for buff in BufReader::new(file).lines() {
    let mut line: &str = &buff.unwrap();
    let mut array: Vec<&str> = line.split(' ').collect();
    let mut numbers: Vec<i64> = Vec::new();

    for x in &array {
      let mut string_number: String = x.to_string();
      let mut number_conver: i64 = string_number.parse::<i64>().unwrap();
      numbers.push(number_conver);
    }

    if numbers.len() > 1 {
      let mut positions: Vec<i64> = Vec::new();
      for d in 0..numbers.len() {
        let mut sum = numbers[d];
        while sum > 0 {
          let mut result = sum % 10;
          sum /= 10;
          positions.push(result);
        }

        let mut sum_o = 0;
        let mut cont = 1;
        for x in (positions.iter()).rev() {
          let mut operation = x * cont;
          sum_o += operation;
          cont += 1;
        }
        print!("{} ", sum_o);
        for t in 0..positions.len() {
          positions.pop();
        }
      }
    }
  }
}

/*
$ ./santiyepes
0 18 10 83 12 2 29 13 90 47 35 8 109 4 67 56 17 123 152
174 32 187 96 77 84 212 83 6 19 88 45 16 8 18 24 25 119 5 23 16
*/
