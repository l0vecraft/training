#!/usr/bin/perl
# $perl dferrans.pl #perl has built-in linting
# use strict; use warnings;
use strict;
use warnings;

my @array = ();
my $counter = 0;
my @finalresult = ();

foreach my $line ( <STDIN> ){
    my $answer = $line ;
    my @result = ();
    my $dividend = 2;
    if ($counter > 0){

       while ($answer > 2){

           my $modulo = $answer % $dividend;
           if($modulo != 0){
             $dividend++;
           }

           if($modulo == 0){
              $answer = $answer / $dividend;
              push(@result,  $dividend);
          }

       }

        push(@finalresult,  join("*",@result));
    }
    $counter ++;
}

print join(" ", @finalresult);
# $ perl dferrans.pl < DATA.lst
# 73*109*271*521*587 83*223*311*487*577 103*317*401*521*541 229*349*509*557
# 173*227*379*463*479 61*97*179*337*491 71*107*173*433*523 139*241*331*331*421
# 53*73*139*293*421 59*137*251*479*541 199*337*389*421*523 79*157*317*367*563
# 359*401*541*563 53*263*367*389*487 101*157*241*349*587 101*109*173*379*503
# 277*313*419*439*491 97*199*229*263*269 181*281*367*373*383 61*101*151*199*211
# 389*397*439*521 127*127*131*197*211 151*229*347*383*439 173*293*337*443*467
# 239*307*373*449*449 59*211*383*431*563
