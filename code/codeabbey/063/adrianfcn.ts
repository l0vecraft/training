/*
$ tslint adrianfcn.ts
$ tsc adrianfcn.ts
*/
import * as fs from "fs";

function genPrimList(n) {
  const natureNumbers: number[] = [];
  const primeList: number[] = [];
  for (let i = 2; i < n + 1; i++) {
    natureNumbers.push(i);
  }
  for (const i of natureNumbers) {
    const vl = i;
    if (Math.pow(vl, 2) <= n && vl !== 0) {
      for (let j = 2; j < (n / vl) + 1; j++) {
        natureNumbers[(vl * j) - 2] = 0;
      }
    }
  }
  for (const i of natureNumbers) {
    if (i > 0) {
      primeList.push(i);
    }
  }
  return primeList;
}

function input() {
  const data = fs.readFileSync("DATA.lst", "utf8");
  const arr = data.split("\n");
  const inputs: number[] = [];
  arr.pop();
  arr.shift();
  for (const i of arr) {
    const v = parseInt(i, 10);
    inputs.push(v);
  }
  return inputs;
}

function integerFactor(n) {
  const listPrime = genPrimList(999);
  let div = n;
  let i = 0;
  let output = "";
  while (div !== 1) {
    const mod = div % listPrime[i];
    const d = div / listPrime[i];
    if (mod === 0) {
      output += listPrime[i];
      if (d !== 1) {
        output += "*";
      }
      div = d;
    } else {
      i++;
    }
  }
  return output;
}

function main() {
  const inputs = input();
  let answer = "";
  for (const i of inputs) {
    answer += " " + integerFactor(i);
  }
  // tslint:disable-next-line:no-console
  console.log(answer);
}

main();

/*  node adrianfcn.js
  73*109*271*521*587 83*223*311*487*577 103*317*401*521*541
  229*349*509*557 173*227*379*463*479 61*97*179*337*491 71*107*173*433*523
  139*241*331*331*421 53*73*139*293*421 59*137*251*479*541 199*337*389*421*523
  79*157*317*367*563 359*401*541*563 53*263*367*389*487 101*157*241*349*587
  101*109*173*379*503 277*313*419*439*491 97*199*229*263*269
  181*281*367*373*383 61*101*151*199*211 389*397*439*521 127*127*131*197*211
  151*229*347*383*439 173*293*337*443*467 239*307*373*449*449
  59*211*383*431*563
*/
