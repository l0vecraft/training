/*
Linting with CppCheck assuming the #include files are on the same
folder as kedavamaru.cpp
> cppcheck --enable=all --inconclusive --std=c++14 kedavamaru.cpp
Checking kedavamaru.cpp ...

Compiling and linking using the "Developer Command Prompt for VS 2017"
> cl /EHsc kedavamaru.cpp

Microsoft (R) C/C++ Optimizing Compiler Version 19.15.26726 for x86
Copyright (C) Microsoft Corporation.  All rights reserved.

kedavamaru.cpp

Microsoft (R) Incremental Linker Version 14.15.26726.0
Copyright (C) Microsoft Corporation.  All rights reserved.

/out:kedavamaru.exe
kedavamaru.obj
*/
/*
Curious fact 1:
  This is exactly the same code that I used to solve
  Codeabbey 69 - Fibonacci divisibility, it turns out that my code
  was good enough to beat both problems

Curious fact 2:
  Fibonnaci number #1015890 (one of the answers) has 212308 digits
  in base 10. This algorithm never uses more than 10 digits in base 10
  to compute that answer.
*/
/*
There are not existing solutions on the training repository. Moreover,
existing solutions on OTHERS.lst focuses on the naive approach,
that is:
  Compute fibonacci sequence, and brute force until f % n == 0

That's why until now this problem only has been solved in languages with
big integer support, like python and java in OTHERS.lst.

However life is tough for c++ programmers since we don't have
a big integer library included in the standard library and the fibonacci
sequence is going to extend until numbers of many many digits that easily
surpass our 64 bit integers.

Luck is that, with a bit of math you can solve the problem even faster
than in a programming language with big integer support. Faster in the
sense that you don't need to compute f % n where f have a thousand
digits which is inherently slow, but you can just compute f' % n, where
f' have no more digits than the digits of n itself + 1. As long as n can be
stored in a 31 bits integer this code works just fine, and can be extended
to 63 bits integers changing one line of code.

Took me some time to realize that...

This is my approach:
  Define a modular (in n) fibonacci sequence:
    fc = (fa + fb) % n;
  This transformation has the propperty that divisibility in n is preserved
    Homework: prove it
  Find (fc == 0)

Thank you Leonard Euler!
*/

#include <iostream>
#include <fstream>
#include <vector>

using namespace std;
// C++14 standard guarantees unsigned int is at least 32 bits
using uint = unsigned int;

int main() {
  ifstream file("DATA.lst");
  if (!file) return 1;

  uint ncases;
  file >> ncases;

  while (ncases--) {
    uint n;
    file >> n;

    for (uint i = 1, fa = 0, fb = 1, fc;;) {
      ++i;

      fc = (fb + fa) % n;
      fa = fb;
      fb = fc;

      if (fc == 0) {
        cout << i << " ";
        break;
      }
    }
  }
}

/*
Running using the "Windows Command Prompt", assumming "DATA.lst" is
on the same folder as kedavamaru.exe

> kedavamaru.exe
71100 123000 1400 28290 60414 233172 3480 857744 54420 320630 29160 1015890
 118608 73308 55836 12654 101610 172533 68100
*/
