/*
  $ eslint adrianfcn.js
*/


function swap(copy, left, right) {
  if (copy.length === 1) {
    return copy;
  }
  copy.splice(left, 1, copy.splice(right, 1, copy[left])[0]);
  return copy;
}

function partition(copy, left, right) {
  let dir = 'left';
  const pivot = copy[left];
  while (left < right) {
    if (dir === 'left') {
      if (copy[right] > pivot) {
        right--;
      } else {
        swap(copy, left, right);
        left++;
        dir = 'rightt';
      }
    }
    if (copy[left] < pivot) {
      left++;
    } else {
      swap(copy, right, left);
      right--;
      dir = 'left';
    }
  }
  copy[left] = pivot;
  return left;
}

function quicksort(copy, low, high) {
  if (low < high) {
    const pivot = partition(copy, low, high);
    process.stdout.write(`${ low }-${ high } `);
    quicksort(copy, low, pivot - 1);
    quicksort(copy, pivot + 1, high);
  }
}

function inputs(unt, cont) {
  const contents = cont.split('\n');
  contents.pop();
  contents.shift();
  const input = contents.join(' ')
    .split(' ')
    .map((value) => (parseInt(value, 10)));
  quicksort(input, 0, input.length - 1);
}

const file = require('fs');
function fileLoad() {
  return file.readFile('DATA.lst', 'utf8', (mistake, contents) =>
    inputs(mistake, contents)
  );
}

fileLoad();

/*
  $ node adrianfcn.js
  0-132 1-132 1-54 1-39 1-19 1-3 1-2 5-19 7-19 7-11 7-9 7-8 13-19 13-16 13-14
  18-19 21-39 21-28 21-24 22-24 23-24 26-28 26-27 30-39 30-34 30-33 32-33
  36-39 37-39 38-39 41-54 41-45 41-44 42-44 42-43 47-54 47-48 50-54 52-54
  52-53 56-132 56-66 56-61 56-60 56-59 57-59 58-59 63-66 63-65 63-64 68-132
  68-103 68-100 68-75 68-71 69-71 70-71 73-75 77-100 79-100 79-90 79-85 79-84
  81-84 81-82 87-90 87-88 92-100 92-93 95-100 95-97 99-100 102-103 105-132
  107-132 107-109 107-108 111-132 111-113 112-113 115-132 116-132 116-123
  116-118 117-118 120-123 120-121 125-132 125-128 125-127 126-127 130-132
  130-131
*/
