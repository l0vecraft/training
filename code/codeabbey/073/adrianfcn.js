/*
  $ eslint adrianfcn.js
*/
function distance(xone, yone, xtwo, ytwo) {
  return Math.sqrt(Math.pow(xtwo - xone, 2) + Math.pow(ytwo - yone, 2));
}

function solution(steps) {
  const val = 0.866025404;
  const magic = 0.5;
  let x = 0.0;
  let y = 0.0;
  for (let i = 0; i < steps.length; i++) {
    switch (steps[i]) {
      case 'A':
        x += 1;
        break;
      case 'B':
        x += magic;
        y += val;
        break;
      case 'C':
        x -= magic;
        y += val;
        break;
      case 'D':
        x -= 1;
        break;
      case 'E':
        x -= magic;
        y -= val;
        break;
      case 'F':
        x += magic;
        y -= val;
        break;
      default:
    }
  }
  return distance(0, 0, x, y);
}

function input(unt, cont) {
  const content = cont.split('\n');
  content.pop();
  content.shift();
  let sol = '';
  const inputs = content.join(' ')
    .split(' ');
  for (let i = 0; i < inputs.length; i++) {
    sol += `${ solution(inputs[i]) } `;
  }
  return process.stdout.write(`${ sol }\n`);
}

const file = require('fs');
function fileLoad() {
  return file.readFile('DATA.lst', 'utf8', (mistake, contents) =>
    input(mistake, contents)
  );
}

fileLoad();

/*
  $ node adrianfcn.js
 0 4.582575695974271 3.605551275929975 4 3.464101615353316 6.24499799851797
 2.645751311699622 1.732050807676658 1.7320508079999999 1.0000000001866816
 5.2915026222703 1.0000000001866816 5.2915026222703 1.0000000001866816
 3.000000000560045 1.0000000001866816 2 1.732050807676658 2.64575131113515
 6.557438525326874 3.605551275929975 3.4641016153533157 0 1.732050807676658
*/
