/**
 * $ dartanalyzer --analysis_options.yaml < dferrans.dart # linting
 *  analysis_options.yaml
 *  (linter: rules:- annotate_overrides- - prefer_is_not_empty hash_and_equals)
 * $ No issues found!
 */
import 'dart:io';

void main() async {
 List<String> data = await new File( 'DATA.lst').readAsLines();
 var answer = [];
    for (var i = 0; i < data.length; i++) {
          if(i >0){
              var values = data[i].split(' ');
              var newlist = values.map((val) => int.parse(val)).toList();
              var A = newlist[0] / 100;
              var B = newlist[1]/100;
              var result = (A / ((A + B) - (A*B))) * 100;
              answer.add(result.round());
          }
    }
print(answer.join(" "));
}
/**
 * $ dart dferrans.dart
 * $ 48 46 94 72 89 72 71 73 58 50 55 52 87 85 76 96 66 88 35 93 82 61 74 40
 **/
