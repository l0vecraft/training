###
  $ coffeelint adrianfcn.coffee
  ✓ adrianfcn.coffee

✓ Ok! » 0 errors and 0 warnings in 1 file

###
fs = require 'fs'

yach_or_dice_poker = (values) ->
  pos_values = [0,0,0,0,0,0]
  res = [0,0,0,0,0,0]

  for i in [1..6]
    for j in [0..values.length]
      if values[j] == i
        pos_values[i - 1]++

  for i in [0..pos_values.length]
    if pos_values[i] == 2
      res[0]++
    else if pos_values[i] == 3
      res[1]++
    else if pos_values[i] == 4
      res[2]++
    else if pos_values[i] == 5
      res[3]++
    else if pos_values[i] == 1 && pos_values[5] != 1
      res[4]++
    else if pos_values[i] == 1 && pos_values[0] != 1
      res[5]++

  if res[0] == 1 && res[1] == 1
    sol = "full-house"
  else if res[0] == 1
    sol = "pair"
  else if res[1] == 1
    sol = "three"
  else if res[2] == 1
    sol = "four"
  else if res[3] == 1
    sol = "yacht"
  else if res[0] == 2
    sol = "two-pairs"
  else if res[4] == 5
    sol = "small-straight"
  else if res[5] == 5
    sol = "big-straight"
  else
    sol = "none"

  return sol

solution = (dat) ->
  line = dat.split("\n")
  sol = ""
  for l in line
    values = []
    for v in l.split(" ")
      values.push(parseInt(v, 10))
    if l.length > 2
      sol += yach_or_dice_poker(values) + " "
  console.log(sol)
main = () ->
  fs.readFile('DATA.lst', (err, dat) -> solution(dat.toString()))

main()

###
 $ coffee adrianfcn.coffee
  four pair pair small-straight small-straight small-straight yacht
  small-straight small-straight big-straight pair pair two-pairs
  small-straight two-pairs pair pair big-straight small-straight
  two-pairspair two-pairs small-straight two-pairs two-pairs pair pair
###
