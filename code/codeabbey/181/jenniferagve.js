/*  Feature: Solving the challenge #181 Reverse Polish Notation
  With Python v3
  From http://www.codeabbey.com/index/task_view/reverse-polish-notation

    Linting:   eslint jenniferagve.py
    --------------------------------------------------------------------
    92:1  error  Unused expression  fp/no-unused-expression
    ✖ 1 problem (1 error, 0 warnings)
*/
function operation(value, firstvar, secondvar) {
  /* On this function math operations are made taking 2 or 1 value */
  if (value === 'mul') {
    const result = parseFloat(firstvar) * parseFloat(secondvar);
    return result;
  } else if (value === 'add') {
    const result = parseFloat(firstvar) + parseFloat(secondvar);
    return result;
  } else if (value === 'sub') {
    const result = parseFloat(firstvar) - parseFloat(secondvar);
    return result;
  } else if (value === 'div') {
    const result = parseFloat(firstvar) / parseFloat(secondvar);
    return result;
  } else if (value === 'sqrt') {
    const result = Math.sqrt(parseFloat(firstvar));
    return result;
  } else if (value === 'mod') {
    const result = parseFloat(firstvar) % parseFloat(secondvar);
    return result;
  }
  return true;
}

function posibledata(element) {
  /* The function recognize which operator is used */
  if (element === 'mul' || element === 'add' || element === 'sub') {
    return [ true, 2 ];
  } else if (element === 'sqrt') {
    return [ true, 1 ];
  } else if (element === 'div' || element === 'mod') {
    return [ true, 2 ];
  }
  return [ false, 0 ];
}

function calculus(datainfofixes) {
  /* This function goes through the data, taking and defining the
  values from the array to calculate the values */
  const findop = datainfofixes.map((element) => posibledata(element));
  const quantity = findop.map((element) => element[1]);
  const findOperation = findop.map((element) => element[0]);
  const numberofop = findOperation.indexOf(true);
  const firstpart = datainfofixes.slice(0, numberofop - quantity[numberofop]);
  const newvalue = String(operation(datainfofixes[numberofop],
    datainfofixes[numberofop - quantity[numberofop]],
    datainfofixes[numberofop - (quantity[numberofop] - 1)]));
  const secondpart = datainfofixes.slice(numberofop + 1);
  const concatena = firstpart.concat(newvalue, secondpart);
  return concatena;
}

function recall(totallength, datainfofixes) {
  /* Use the function calculus to make the operation and
  repeat it till the end */
  const arrayResult = calculus(datainfofixes);
  totallength = arrayResult.length;
  if (totallength === 1) {
    return arrayResult;
  }
  return recall(totallength, arrayResult);
}

/* eslint no-sync: ["error", { allowAtRootLevel: true }] */
const filesre = require('fs');
const contents = filesre.readFileSync('DATA.lst', 'utf8', '\n');
function fileprocess() {
/* Read the line of the file and save it to be process */
  const datainfofixes = contents.toString().split(' ');
  return datainfofixes;
}

function main() {
/* Main where each function is called to calculate polish reversed */
  const datainfofixes = fileprocess();
  const totallength = datainfofixes.length;
  const result = recall(totallength, datainfofixes);
  const resultFix = String(result);
  return resultFix;
}

const resultFix = main();
process.stdout.write(`${ resultFix } \n`);

/**
    node jenniferagve.js
    input:70 11 mul 5 div 219 add 28 26 6 sub 6 sub div mul 448 7 mul sqrt add
    -------------------------------------------------------------------
    output: 802
*/
