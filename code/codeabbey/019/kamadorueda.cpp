/*
Linting with CppCheck assuming the #include files are on the same
folder as kedavamaru.cpp
> cppcheck --enable=all --inconclusive --std=c++14 kedavamaru.cpp
Checking kedavamaru.cpp ...

Compiling and linking using the "Developer Command Prompt for VS 2017"
> cl /EHsc kedavamaru.cpp

Microsoft (R) C/C++ Optimizing Compiler Version 19.15.26726 for x86
Copyright (C) Microsoft Corporation.  All rights reserved.

kedavamaru.cpp

Microsoft (R) Incremental Linker Version 14.15.26726.0
Copyright (C) Microsoft Corporation.  All rights reserved.

/out:kedavamaru.exe
kedavamaru.obj
*/

#include <iostream>
#include <fstream>
#include <stack>

using namespace std;

int main() {
  ifstream file("DATA.lst");
  if (!file) return 1;

  int ncases; file >> ncases;
  file.get();

  bool isok = true;
  stack<char> bs;
  while(!file.eof()) {
    char c = file.get();

    if ((c == '\n') || file.eof()) {
      if (isok && bs.empty()) cout << "1 ";
      else cout << "0 ";
      isok = true;
      bs = stack<char>();
      continue;
    }
    if (!isok) continue;

    if (c == '(') bs.push(c);
    if (c == '[') bs.push(c);
    if (c == '{') bs.push(c);
    if (c == '<') bs.push(c);

    if (c == ')') {
      if (!bs.empty() && (bs.top() == '(')) bs.pop();
      else isok = false;
    }
    if (c == ']') {
      if (!bs.empty() && (bs.top() == '[')) bs.pop();
      else isok = false;
    }
    if (c == '}') {
      if (!bs.empty() && (bs.top() == '{')) bs.pop();
      else isok = false;
    }
    if (c == '>') {
      if (!bs.empty() && (bs.top() == '<')) bs.pop();
      else isok = false;
    }
  }
}

/*
Running using the "Windows Command Prompt"
assuming "kedavamaru.exe" and "DATA.lst" are on the same folder

> kedavamaru.exe
0 1 1 0 0 1 0 1 1 0 0 0 0 0 1 0 0 0 1
*/
