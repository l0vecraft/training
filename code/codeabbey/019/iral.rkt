;; $ racket -I typed/racket
;; $ raco exe --gui iral.rkt

#lang typed/racket

(define data (open-output-file "DATA.lst"))

(define (check a b)
  (let ([ai (char->integer a)]
        [bi (char->integer b)])
    (not (or (= (- ai 1) bi) (= (- ai 2) bi)))))

(for ([i (string->number (string-split (read-line data)))])
  (printf "~A " (let loop ([stack '()]
                           [chars (string->list (read-line))])
                  (cond [(empty? chars) (or (and (empty? stack) 1) 0)]
                        [(member (car chars) '(#\) #\} #\] #\>))
                         (if (or (empty? stack)
                                 (check (car chars) (car stack)))
                             0
                             (loop (cdr stack) (cdr chars)))]
                        [(member (car chars) '(#\( #\{ #\[ #\<))
                         (loop (cons (car chars) stack) (cdr chars))]
                        [else (loop stack (cdr chars))]))))

(close-output-port data)

;; ./iral
;; 0 1 1 0 0 1 0 1 1 0 0 0 0 0 1 0 0 0
