"""
This code calculates the hamming distances for diferent strings
>pylint juanes9327.py

    -------------------------------------------------------------------
    Your code has been rated at 10.00/10

"""


from __future__ import print_function


def distance(strand_a, strand_b):

    """
    This function shows the hamming distances of two strings
    """
    length_a = len(strand_a)
    length_b = len(strand_b)
    if length_a != length_b:
        raise ValueError("The strings must be the same length")
    else:
        list_strand_a = list(strand_a)
        list_strand_b = list(strand_b)
        distance_hamming = 0
        for i in range(length_a):
            if list_strand_a[i] != list_strand_b[i]:
                distance_hamming = distance_hamming + 1
    print(distance_hamming)


DATA = open("DATA.lst", "r")
LIST_DATA = DATA.readlines()
for count in range(0, len(LIST_DATA), 2):
    string_a = LIST_DATA[count]
    string_b = LIST_DATA[count+1]
    distance(string_a, string_b)

DATA.close()


# > python juanes9327.py
# 0
# 0
# 9
