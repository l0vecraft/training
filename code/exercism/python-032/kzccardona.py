"""
This code find the difference between the square of the sum and
The sum of the squares of the first n natural numbers
pylint kzccardona.py

    -------------------------------------------------------------------
    Your code has been rated at 10.00/10

"""
from __future__ import print_function


def square_of_sum(number):

    """
    This function finds the square of the sum from 1 to number
    """
    count = 0
    for xsum in range(1, number + 1):
        count = count + xsum
    return count ** 2


def sum_of_squares(number):

    """
    This function finds the sum of squares from 1 to number
    """
    count = 0
    for xsum in range(1, number + 1):
        count = count + xsum ** 2
    return count


def difference_of_squares(number):

    """
    This function finds the difference between the square of
    The sum and the sum of the squares
    """
    return square_of_sum(number) - sum_of_squares(number)


for numbers in open("DATA.lst", "r"):
    num = int(numbers.rstrip())
    if num > 0 and isinstance(num, int):
        print("Square of sum:", square_of_sum(num))
        print("Sum of squares:", sum_of_squares(num))
        print("Difference of squares:", difference_of_squares(num))
    else:
        print("You must type a natural number that is greater than 0")


# > python ./kzccardona.py
# Square of sum: 25502500
# Sum of squares: 338350
# Difference of squares: 25164150
# Square of sum: 404010000
# Sum of squares: 2686700
# Difference of squares: 401323300
# Square of sum: 225
# Sum of squares: 55
# Difference of squares: 170
