#!/usr/bin/env python3
'''
$pylint alejandrohg7.py
Your code has been rated at 10.00/10 (previous run: 10.00/10, +0.00)
'''

from __future__ import print_function


def open_file():
    '''This function open and reads a file'''
    with open('DATA.lst', 'r') as opened_file:
        rational_numbers = []
        for line in opened_file:
            rational_numbers.append(line.split())
        return rational_numbers


def highest_common_factor(nume_gdc, deno_gdc):
    """find GCD using Euclidean algo"""
    while deno_gdc:
        nume_gdc, deno_gdc = deno_gdc, nume_gdc % deno_gdc
    return nume_gdc


def __add__(r_n_l):
    """this function summ rationals"""
    nume_a1 = int(r_n_l[0])
    deno_b1 = int(r_n_l[1])
    nume_a2 = int(r_n_l[3])
    deno_b2 = int(r_n_l[4])
    numerator = (nume_a1 * deno_b2) + (nume_a2 * deno_b1)
    denominator = deno_b1 * deno_b2
    h_c_f = highest_common_factor(numerator, denominator)
    numerator_hcf = numerator/h_c_f
    denominator_hcf = denominator/h_c_f
    return (int(numerator_hcf), int(denominator_hcf))


def __sub__(r_n_l):
    """this function subtract rationals"""
    nume_a1 = int(r_n_l[0])
    deno_b1 = int(r_n_l[1])
    nume_a2 = int(r_n_l[3])
    deno_b2 = int(r_n_l[4])
    numerator = (nume_a1 * deno_b2) - (nume_a2 * deno_b1)
    denominator = deno_b1 * deno_b2
    h_c_f = highest_common_factor(numerator, denominator)
    numerator_hcf = numerator/h_c_f
    denominator_hcf = denominator/h_c_f
    return (int(numerator_hcf), int(denominator_hcf))


def __mul__(r_n_l):
    """this function multiply rationals"""
    nume_a1 = int(r_n_l[0])
    deno_b1 = int(r_n_l[1])
    nume_a2 = int(r_n_l[3])
    deno_b2 = int(r_n_l[4])
    numerator = (nume_a1 * nume_a2)
    denominator = (deno_b1 * deno_b2)
    h_c_f = highest_common_factor(numerator, denominator)
    numerator_hcf = numerator/h_c_f
    denominator_hcf = denominator/h_c_f
    return (int(numerator_hcf), int(denominator_hcf))


def __div__(r_n_l):
    """this function divide rationals"""
    nume_a1 = int(r_n_l[0])
    deno_b1 = int(r_n_l[1])
    nume_a2 = int(r_n_l[3])
    deno_b2 = int(r_n_l[4])
    numerator = (nume_a1 * deno_b2)
    denominator = (nume_a2 * deno_b1)
    h_c_f = highest_common_factor(numerator, denominator)
    if denominator != 0:
        numerator_hcf = numerator/h_c_f
        denominator_hcf = denominator/h_c_f
    return (int(numerator_hcf), int(denominator_hcf))


def __abs__(r_n_l):
    """this function gives absolute value to rationals"""
    nume_a1 = int(r_n_l[0])
    deno_b1 = int(r_n_l[1])
    numerator = abs(nume_a1)
    denominator = abs(deno_b1)
    h_c_f = highest_common_factor(numerator, denominator)
    numerator_hcf = numerator/h_c_f
    denominator_hcf = denominator/h_c_f
    return (int(numerator_hcf), int(denominator_hcf))


def __pow__(r_n_l):
    """this function raise a rational"""
    nume_a1 = int(r_n_l[0])
    deno_b1 = int(r_n_l[1])
    power = int(r_n_l[3])
    if power >= 0:
        numerator = nume_a1 ** power
        denominator = deno_b1 ** power
        h_c_f = highest_common_factor(numerator, denominator)
        numerator_hcf = numerator/h_c_f
        denominator_hcf = denominator/h_c_f
    elif power < 0:
        numerator = nume_a1 ** abs(power)
        denominator = deno_b1 ** abs(power)
        h_c_f = highest_common_factor(numerator, denominator)
        numerator_hcf = numerator/h_c_f
        denominator_hcf = denominator/h_c_f
    return (int(denominator_hcf), int(numerator_hcf))


def __rpow__(r_n_l):
    """this function raise a real numbers"""
    real_nume = int(r_n_l[0])
    power_nume = int(r_n_l[2])
    power_deno = int(r_n_l[3])
    real_raised_to_rational = real_nume**(power_nume/power_deno)
    return float(round(real_raised_to_rational, 8))


def __reduct__(r_n_l):
    """this function reduce to lowest terms a rational"""
    nume_a1 = int(r_n_l[0])
    deno_b1 = int(r_n_l[1])
    numerator = nume_a1
    denominator = deno_b1
    h_c_f = highest_common_factor(numerator, denominator)
    numerator_hcf = numerator/h_c_f
    denominator_hcf = denominator/h_c_f
    return (int(numerator_hcf), int(denominator_hcf))


for each_case in open_file():
    if 'addition' in each_case:
        print(__add__(each_case))
    elif 'subtraction' in each_case:
        print(__sub__(each_case))
    elif 'multiplication' in each_case:
        print(__mul__(each_case))
    elif 'division' in each_case:
        print(__div__(each_case))
    elif 'absolute' in each_case:
        print(__abs__(each_case))
    elif 'exponentiation' in each_case:
        print(__pow__(each_case))
    elif 'exponentiationrealnumber' in each_case:
        print(__rpow__(each_case))
    elif 'reduction' in each_case:
        print(__reduct__(each_case))
# $ python3 alejandrohg7.py
# (7, 6)
# (-1, 6)
# (-7, 6)
# (0, 1)
# (-1, 6)
# (7, 6)
# (1, 6)
# (0, 1)
# (1, 3)
# (-1, 3)
# (1, 3)
# (1, 1)
# (1, 2)
# (0, 1)
# (3, 4)
# (-3, 4)
# (3, 4)
# (1, 2)
# (1, 2)
# (1, 2)
# (1, 2)
# (1, 2)
# (0, 1)
# (8, 1)
# (8, -1)
# (1, 0)
# (1, 1)
# (1, 1)
# (1, 1)
# 16.0
# 0.33333333
# 1.0
# (1, 2)
# (-2, 3)
# (-1, 3)
# (0, 1)
# (-2, 1)
# (1, 1)
