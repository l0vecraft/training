"""
This code process a matrix and extract his rows and columns
pylint kzccardona.py

    -------------------------------------------------------------------
    Your code has been rated at 10.00/10

"""
from __future__ import print_function


class Matrix(object):

    """
        This class process a matrix and extracts his rows and columns
    """
    def __init__(self, matrix_string):

        """
        This function initialize matrix_string
        """
        self.matrix_string = matrix_string

    @classmethod
    def row(cls, index):

        """
        This function extracts the specific row from an matrix
        """
        row = []
        try:
            for xrow in range(len(MATRIX.matrix_string[0])):
                row.append(MATRIX.matrix_string[index-1][xrow])
        except IndexError as error:
            print(error)
        return row

    @classmethod
    def column(cls, index):

        """
        This function extracts the specific column from an matrix
        """
        col = []
        try:
            for xcol in range(len(MATRIX.matrix_string)):
                col.append(MATRIX.matrix_string[xcol][index-1])
        except IndexError as error:
            print(error)
        return col


MATRIX = ()
COL = 0
ROW = 0
TMP_LIST = []
TMP_ROW = []
DATA = []
for numbers in open("DATA.lst", "r"):
    DATA.append(numbers.rstrip('\n'))

MATRIX = DATA[0].replace('\\n', '\n')
ROW = int(DATA[1])
COL = int(DATA[2])

if MATRIX:
    for element in enumerate(MATRIX.splitlines()):
        TMP_ROW = element[1].split(' ')
        TMP_LIST.append(TMP_ROW)
        TMP_ROW = []

MATRIX = Matrix(TMP_LIST)
print(MATRIX.row(ROW))
print(MATRIX.column(COL))


# > python ./kzccardona.py
# ['7', '8', '9']
# ['2', '5', '8', '7']
