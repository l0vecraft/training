
###
coffeelint henrymej.cofee
###
fs = require 'fs'

solve = (a, b) ->
  prod = 2
  i = 0
  while i<a
    prod = (prod * prod) % b
    i += 1
  return prod

hackerrank = ->
  fs.readFileSync "DATA.lst", 'utf8'

ab = hackerrank().split(' ')
a = parseInt(ab[0], 10)
b = parseInt(ab[1], 10)
result = solve(a, b)
console.log(result)

###
coffee henrymej.coffee
256
###
