
###
coffeelint henrymej.cofee
###

fs = require 'fs'

bin = (dec, digits) ->
  return dec.toString(2)

verify = (num, y) ->
  ones = 0
  len = num.length
  i = 0
  while i < len
    if num[i] == '1'
      ones += 1
    i += 1
  return (ones == y)

increment = (num, digits, y) ->
  candidate = parseInt(num, 2) + parseInt('1', 2)
  while !(verify(bin(candidate, digits), y))
    candidate += parseInt('1', 2)
  return bin(candidate, digits)

translate = (binary, digits) ->
  r = digits - binary.length
  prefix = Array(r + 1).join '0'
  string = prefix.concat(binary)
  result = ''
  i = 0
  while i < digits
    if string[i] == '0'
      result = result.concat('H')
    else
      result = result.concat('V')
    i += 1
  return result

paths = []

x = 0
while x < 10
  paths.push []
  y = 0
  while y < 10
    current = (Array(x + 1).join '0').concat(Array(y + 1).join '1')
    proper = []
    digits = x + y
    comparer = (Array(y + 1).join '1').concat(Array(x + 1).join '0')
    while current < comparer
      proper.push translate(current, digits)
      current = increment(current, digits, y)
      proper.push translate(current, digits)
    paths[x].push proper
    y += 1
  x += 1

solve = (x, y, k) ->
  return paths[x][y][k]

hackerrank = ->
  fs.readFileSync 'DATA.lst', 'utf8'

doc = hackerrank().split('\n')
tc = parseInt(doc[0], 10)
i = 1
solution = ''
loop
  x = parseInt(doc[i].split(' ')[0], 10)
  y = parseInt(doc[i].split(' ')[1], 10)
  k = parseInt(doc[i].split(' ')[2], 10)
  solution =  solution.concat(solve x, y, k).concat(' ')
  if i == tc
    break
  i += 1
console.log(solution)

###
coffee henrymej.coffee
HVVHHHHVVHHHHV VVHHVVVVHVHHVHVH VVVHHHVVVVVHHVH VVHHHHVVHHHHHVVVV
HHHVHHHVVVHHVV VVVVHHHHHVVVVHVHH HHHVVVHHHVV HHHHVVVVVVVHHVHVH
VVVVHHHHVHHVHVVV HVVHVHHHVHVH VHVVHHHHVHHV VVHVVVHVHVHVHHHV
HHVVHVHVHVHVHVV HHHVVVHVVVVH VVVVHHVHHVHHV VHHVHVHHHVHHVV
VVHHHVVHVVHHVH HVHVHVVHHVVVV HHHHHVHVHHVVVH VVHVVHHVVHHHV
VHHHVHVVVVVVHV VVVHHHHVVVHVVHVHH VHVVVHHHVHV HVHVHHVHVVHVH
VVHVHVVHHH HHVHVVVHHV VHHVVHHHHHVVHHVV HVHVHHVHVHV VHHHHVVHHVVHVVHVVH
HVVVHHHVHHHVV VHHVVHVVVHVHHHVH HHVVVVVVHVVHVHHHH HHHVHVHVVVVHVVHVH
HHVVHVHVVHVHVV HVHHHVVVVHHHHHV HVVVHVVHHHHVVVHHH VHHHHHVVVVHHVV
VVHHHHHVVHHVHVVVH HVHVVVVHVVHHV VVHHVHHVVHVV VHHHVVVVHH VVVVHHHVVVHHVHV
VVHVVHHHVHHHVVH HVVVVHHHVHHV VHHHVVHHHVHHVVV HVHVVVHVVVHHV VVHHVHHHHVV
HHVHVHVHHHVVVH HVVVHVHVHHVH HVVVHVHHVHVVH
###
