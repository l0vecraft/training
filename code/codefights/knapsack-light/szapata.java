/*
$java -jar checkstyle-8.23-all.jar -c google_checks-modif.xml szapata.java
Starting audit...
Audit done.
$javac szapata.java
*/

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Scanner;

/*
 *You found two items and must chose the ones that make the most.
 *value out of your weight capabilities
 * @author sebastian
 */
public class szapata {

  /**
   * the running application.
   *
   * @param args args
   * @throws IOException because of the file reading
   */
  public static void main(String []args) throws IOException {
    FileInputStream in = null;
    try {
      Scanner scanner = new Scanner(new File("DATA.lst"));
      int val1 = scanner.nextInt();
      int weight1 = scanner.nextInt();
      int val2 = scanner.nextInt();
      int weight2 = scanner.nextInt();
      int maxW = scanner.nextInt();
      System.out.println(knapsackLight(val1, weight1, val2, weight2, maxW));
    } catch (FileNotFoundException e) {
      System.out.println("Exeption " + e);
    } finally {

      if (in != null) {

        in.close();
      }
    }


  }

  /**
  * get the max value.
  * @param maxW the maxW that can be carried
  * @return the max value that can be obtained given the conditions
  */
  public static int knapsackLight(int value1, int weight1, int value2,
      int weight2, int maxW) {
    if ((weight1 + weight2) <= maxW) {
      return value1 + value2;
    } else {
      if (value1 > value2 && weight1 <= maxW) {
        return value1;
      } else {
        if (weight2 <= maxW) {
          return value2;
        } else if (weight1 <= maxW) {
          return value1;
        } else {
          return 0;
        }

      }
    }
  }

}

/*
$java szapata
10
*/
