/*
$java -jar checkstyle-8.23-all.jar -c google_checks-modif.xml szapata.java
Starting audit...
Audit done.
$javac szapata.java
*/

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Scanner;

/**Given a sequence of integers as an array, determine
 *whether it is possible to obtain a strictly increasing
 *sequence by removing no more than one element from the array.
 *
 * @author sebastian
 */
public class szapata {

  /**
  * the running application.
  *
  * @param args args
  * @throws IOException because of the file reading
  * @throws InterruptedException for the threads
  */
  public static void main(String []args) throws IOException
    , InterruptedException {
    FileInputStream in = null;
    try {
      Scanner scanner = new Scanner(new File("DATA.lst"));
      int size = scanner.nextInt();
      int []secuence = new int[size];
      for (int i = 0; i < secuence.length;i++) {
        secuence[i] = scanner.nextInt();
      }
      szapata obj = new szapata();
      System.out.println(obj.almostIncreasingSequence(secuence));
    } catch (FileNotFoundException e) {
      System.out.println("Exeption " + e);
    } finally {
      if (in != null) {
        in.close();
      }
    }
  }

  boolean almostIncreasingSequence(int[] sequence) throws InterruptedException {
    Storage.sequence = sequence;
    Execution []executions = new Execution[sequence.length];
    for (int i = 0; i < sequence.length;i++) {
      executions[i] = part(i);
      executions[i].thread.start();
    }
    for (int i = 0;i < sequence.length;i++) {
      executions[i].thread.join();
      if (executions[i].keepsCorrect) {
        return true;
      }
    }
    return false;
  }

  /**
  * creates object and thread for making  a strictly increasing
  * secuence.
  * @return object[thread, and validity of secuence]
  */
  public Execution part(int exclude) {
    Execution execution = new Execution();
    Thread myThread = new Thread() {
        @Override
        public void run() {
            try {
              execution.keepsCorrect = findOneSequence(exclude);
            } catch (Exception e) {
              System.out.print("interrupted");
            }
        }
    };
    execution.thread = myThread;
    return execution;
  }

  /**
  * find secuence strictly increasing.
  * @return is the secuence valid?
  */
  public boolean findOneSequence(int exclude) {
    boolean keepsCorrect = true;
    for (int j = 0;j < (Storage.sequence.length - 1);j++) {
      if (exclude != j) {
        if ((j + 1) == exclude) {
          if ((j + 2) < Storage.sequence.length
          && Storage.sequence[j] >= Storage.sequence[j + 2]) {
            keepsCorrect = false;
            break;
          }
        } else if (Storage.sequence[j] >= Storage.sequence[j + 1]) {
          keepsCorrect = false;
          break;
        }
      }
    }
    return keepsCorrect;
  }

  class Execution {
    public boolean keepsCorrect = true;
    public Thread thread;
  }

  static class Storage {
    public static int[] sequence;
  }

}

/*
$java szapata
true
*/

