#! /usr/bin/env python

'''
$ ./charlie517.py
$ pylint charlie517.py
Your code has been rated at 10.00/10 (previous run: 10.00/10, +0.00)
'''

with open('DATA.lst') as f:
    next(f)
    for line in f:
        TEMPS = map(int, line.split())
print min(TEMPS, key=abs)

# ./charlie517
# 5
