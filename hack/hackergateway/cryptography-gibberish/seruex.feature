## Version 2.0
## language: en

Feature: Cryptography-Hackergateway
  Site:
    https://www.hackergateway.com
  Category:
    Cryptography
  User:
    Seruex
  Goal:
    finding the answer, decypting the string

  Background:
  Hacker's software:hackergateway
    | <Software name> |  <Version>     |
    | Parrot Os       |  3.9 (64 bits) |
    | Firefox         | 56.4 (64 bits) |
  Machine information:
    Given I the challenge url
    #https://www.hackergateway.com/challenges/gibberish
    Then I see the string to decrypt
    And I next do

  Scenario: Fail: decrypt MD5 to Text
    Given I access to "https://md5online.es/"
    And I put the caracteres to decrypt
    And I click on "Descifrador"
    And I see the answer
    #Formato invàlido
    Then I find other page for convert the string
    #https://emn178.github.io/online-tools/md5.html
    And I put the caracteres to decrypt
    Then I see #8eb21eba8775e6c1c771e9f1da14e8e4
    And I put this answer in #https://www.hackergateway.com/challenges/gibberish
    And I see the message #Your answer is wrong!
    Then I conclude that the MD5-text caracter conversion does not work for this

  Scenario: Success : decrypt Base64 to Text
    Given I access to "https://emn178.github.io/online-tools/base64_decode.html"
    Then I put the string
    And I see the message
    #the answer is nintendo
    Then I put this answer (nintendo) in
    #https://www.hackergateway.com/challenges/gibberish
    And I see the message #You solved this challenge!
    And I end this problem
    And solved the challenge
