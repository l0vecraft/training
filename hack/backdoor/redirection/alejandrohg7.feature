## Version 2.0
## language: en

Feature: redirection -web -backdoor
  Site:
    backdoor.sdslabs.co
  Category:
    Web
  User:
    alejandrohg7
  Goal:
    Get the hidden flag in the password form

  Background:
  Hacker's software:
    | <Software name> | <Version>   |
    | Ubuntu          | 18.04.1     |
    | Firefox         | 69.0        |
    | Burp Suite      | 2.1.02      |
  Machine information:
    Given I am accessing the web-page using the browser
    And I access to the challenge page

  Scenario: Fail: password form injection using python syntax
    Given there is a password form in the challenge page
    When I try a sample injection "{{1+1}}" in the password form
    Then there is no result
    Then I can not get the flag

  Scenario: Fail: password form SQL injection
    Given there is a password form in the challenge page
    When I try a sample injection "' OR 1=1 --" in the password form
    Then there is no result
    Then I can not get the flag

  Scenario: Success: request with Burp Suite
    When I look the HTML code of the challenge page
    Then I can see the form code
    """
    <form method="POST" action="handle.php">
      Enter password:<br>
      <input type="password" name="password"><br>
      <input type="submit" value="SUBMIT"><br>
    </form>
    """
    And I try to send the "POST" request with Burp Suit to "handle.php"
    And I enter a random password
    And I get the flag [evidence](img.png)
