## Version 1.0
## language: en

Feature: Stored cross-site scripting
  Site:
    Xss-game
  Category:
    Stored cross-site scripting
  User:
    kzccardona
  Goal:
    Interact with the vulnerable application to save in posts a
    JavaScript code

  Background:
  Hacker's software:
    |  <Software name>  |    <version>   |
    | Microsoft Windows | 10.0.17763.437 |
    | Google Chrome     | 75.0  (64 bit) |
  Machine information:

  Scenario: Fail: Save directly the script
    Given I wrote in the input the following javascript code:
    """
      Hello
        <script>
              alert('Success')
        </script>
    """
    When I save the post
    And reload the page
    Then the page didn't show my alert
    And I can't get the flag

  Scenario: Fail: Save a Html button
    Given I search in web about diferent types of XSS
    When find some information and clues to try other attacks
    And I try with a button with on click action
    """
      Hello
        <button onclick="alert('Success')">Click me</button>
    """
    When I save the post
    And reload the page
    Then the page didn't show my alert
    And I can't get the flag

  Scenario: Success: Using onerror event attribute
    Given I try with a "<img>" tag with onerror attribute
    """
      Hello
        <img src="something" onerror="alert('Success')") <script>
    """
    When I save the post
    And reload the page
    Then it works and show my alert
    Then the page give me the flag
    """
    Congratulations, you executed an alert
      Success
    You can now advance to the next level
    """
