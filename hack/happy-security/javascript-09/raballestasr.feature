# language: en

Feature: Solve the challenge Just get started 5
  From the happy-security.de website
  From the JavaScript category
  With my username raballestas

  Background:
    Given an input
    And a prompt to "enter password"

  Scenario: Succesful solution
    When I look into the source code
    When I copy the code for the validating function into a js source
    And I run it locally with node
    Then I see that the password is made with a 2d array
    And I give the password as characters separated by commas
    Then I solve the challenge
