# language: en

Feature: Solve Debugging challenge 2
  From site Valhalla
  From Debugging Category
  With my username Skhorn

Background:
  Given I am running Debian Stretch GNU/Linux kernel 4.9.0-6-amd64
  And I am using Firefox 52.7.3 (64-bit)
  And I am using VIM - Vi IMproved 8.0
  Given a web site with a Java code to debugg
  And the download link of the source code
  """
  URL: https://halls-of-valhalla.org/beta/challenges/debugging2
  Message: What the hell? My Java code breaks when I try to run
  it? What's wrong and how can I fix it?
  Objective 1: Find the error and fix it
  Evidence: debugging2.java
  """

Scenario: Revieweing and patching the code
  Given the source code of the challenge
  And I need to find out why it is not working as expected
  And I compile/execute to see its behavior

  When I execute it "javac debuggin2.java"
  Then an Exception is thrown "java.lang.NullPointerException"
  And it states it's in line "debugging2.java:40"
  When I look at the line pointed
  And I see the following portion of code:
  """
  35  public static void main(String[] args)
  36  {
  37    Thingy[] thingyLog;
  38    thingyLog = new Thingy[3];
  39
  40    thingyLog[0].increaseWatermelon(17);
  """
  Then I see Thingy[] vector, has not been properly initialized
  And I fix this by initializing each position of the vector
  And using the object is referencing
  """
  37    Thingy[] thingyLog;
  38    thingyLog = new Thingy[3];
  39
  40    thingyLog[0] = new Thingy();
  41    thingyLog[1] = new Thingy();
  42    thingyLog[2] = new Thingy();
  """
  Then I save it
  And I execute the code again "javac debugging2.java"
  And the output comes out:
  # Compiling & Executing
  """
  | Watermelon: 17 Cantaloupe: -3  |
  | Watermelon: 13 Cantaloupe: 2   |
  | Watermelon: 234 Cantaloupe: 33 |
  """
  Then I see is now executing in the inteded way
  And I solve the challenge
