# language: en

Feature: Solve Encryption challenge 3
  From site Valhalla
  From Encryption Category
  With my username Skhorn

  Background:
    Given the fact i have an account on Valhalla site
    And i have Debian 9 as Operating System
    And i have internet access

  Scenario: Succesful Solution I
    Given the link to the challenge
    And a description stating a classic encryption is used
    And ciphertext
    And i search in google for a rail fence decoder
    And i use 4 number of rails
    And decrypt the message
    Then i should have a plain text message
    And i add the appropiate whitespaces
    And use it as an answer
    And i solve the challenge

  Scenario: Succesful Solution II
    Given the link to the challenge
    And a description stating a classic encryption is used
    And ciphertext
    And i search in google for the scytale encryption
    And i pick a pen
    And i pick a piece of paper
    And i separate the message in block of 5 characters vertically
    When i read each column from top to bottom
    Then i should get a plain text message
    And i input this message as an answer with appropiate spacing
    And with it, i solve the challenge
