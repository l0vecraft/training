## Version 2.0
## language: en

Feature: rsa-multiple-recipients-rootme
  Code:
    RSA - Multiple recipients
  Site:
    www.root-me.org
  Category:
    Cryptanalysis
  User:
    kedavamaru
  Goal:
    Break RSA encryption and decrypt the message and capture the flag.

  Background:
  Hacker's software:
    | <Software name> |   <Version>   |
    |    GNU bash     |     4.4.19    |
    |     Python      |   2.7.15 rc1  |

  Machine information:
    Given I am accessing the challenge page
    And the title is RSA - Multiple recipients
    And file "clef0_pub.pem" is provided
    """-----BEGIN PUBLIC KEY-----
    MIIBIDANBgkqhkiG9w0BAQEFAAOCAQ0AMIIBCAKCAQEAvtCUgebFQ43qMwaIZZ+w
    3aO5NJJLhtUi8THUGk/dScoGY0a6cW0rKuawqmVeoccvx4ZyUy4htOpeJoqjIbij
    HF/6saD36DvDeJKZlynZujIkZSN/ywCsaDsWypqkDIRJaFfGAY2Px8WPOX0GVui2
    yFVT+aFPYZZf7OC4Xu7pm4Eov4tM/+Jb54pLJplEMgE41F9KFshBkqtYlfxkpPJ5
    aocjS0jEby34cZ8o79rQIhGGUuPSXkTaPlH8EzROHDq9deQMYklJspN1urNuH/mm
    kt856tyhefJrRBfNXXEgH33u970FMzlBV+uc0pJgLEgUSMPCCjbhiddzOxKsOYiP
    OwIBAw==
    -----END PUBLIC KEY-----
    """
    And file "clef1_pub.pem" is provided
    """-----BEGIN PUBLIC KEY-----
    MIIBIDANBgkqhkiG9w0BAQEFAAOCAQ0AMIIBCAKCAQEAugXwzOVLmHw+2t0g/jhq
    /JG7oF9ryVktXuPU/UMNfcMVA/WFemx1aVtGqpP/eSxVw/f8gUpZtQ8BA3x6Zr2a
    irKBO0Wfx2BPDL8+e0rsqHW2wIBoV1n4TglWonap94nfmmz10y7Q7SjQVzDQaAif
    v6LObE2smiuCCQobhAGaP2meDSa44M0ZuQmAlw0yMb+b4XwS6XCwX0JS2Yt4Rw51
    xhtBV0idQc3acrLg7J9+MQrfuL/yneNEZUy/7nJDKAsfgh/lyQI5qFCqS+Q9UMQg
    I1LMl7IR18tyihQs8t961zmZEZ1HjftnapQ8W3yuMLoOGidOI9TMY1UEtLRUxxf2
    IQIBAw==
    -----END PUBLIC KEY-----

    """
    And file "clef2_pub.pem" is provided
    """-----BEGIN PUBLIC KEY-----
    MIIBIDANBgkqhkiG9w0BAQEFAAOCAQ0AMIIBCAKCAQEAq8dX4iDR8NpG0BfQVCJz
    +q6sqZ1o6jtAD5l1K13r22euMQfY9JH5F7oHnhRwj3AvyPwaYYn8H0HrozRIJpVk
    KvyAg9U92newtYwlGSbtd5RGoTe7xO/jVysNYd/BpSTCx4kabea9pbFbFgQoTMqY
    uX9uYJdMsX8KWxoXvvIAfgL6iovw4Cad5E2sJcfHgmy6wgGUNTc+iHZY5fr3M+CK
    4EoFL6UUR//7O57t9/7sDI5iqw5RxRbbU37pPZ94CyDhZl2OnlQdXo3pqi6hRHtW
    0HdwouW3LKoIkZiNl7d/OCdIMIFL1I4ziACwg3/vQTW62I9ReJQUBs/bgx/hgXGh
    PwIBAw==
    -----END PUBLIC KEY-----

    """
    And file "m0" is provided
    """
    vjXJvWis95tc25G+wxC5agClCJFB9vUslFyV+I
    4bSiwS4Sm6k8eF61EizKo4hZFwROlO3Ci3YQaT
    rAm+Y9/qEbM7asvwKTePKX+cLVN61l0xxfTL8C
    doXkRE2rSczp1AzzmFz83OHgszX/Wf7kgWU4M7
    3+efPvU9FmcWOauakrdJZx8B0ErJ5cYWNS0ZCa
    m0Nlz+pISqdSJ6MSz0Ek62Ulb3ei8I41FOdHtd
    8mhC7dfpdfmVLOSEW4yEnn5iuMW0ydvW055dod
    Lc9RKcvJafH9e3zf8/S1/RORXZoUHWnBXBEFOl
    8iVXz70GcDTPzIhxh6imi0ynLbV68qW2vw2XRQ
    ==
    """
    And file "m1" is provided
    """
    Mi4MnobVabt1+Q7R/0aIYBBpeRxPRuR6gkhr/W
    bw/D23ywu2KbUYBab2XbEguRz8yzBlxScbFjjb
    98DuILxURoFN8lNKVJLS3d/IrGGr0hjocbz27u
    BS97hseX0S4nd+BL6LUn0o7qe/yCqk7Y0tEhhc
    uSMrGn/l9N/6UjgN38TyoyvVbd1UH6BHGLdj9g
    5JRBKAvcGumymfiE0qS7IOnM3mN5W8gRJaEGDk
    hDim21Pm1Yg2GRBJ+z7C8AEy+Dz6OFvWuDsY24
    Gb+643D7KrmFObJ1n8Qyme/Y1bfvBkG+xdvGoB
    zyQrlDT12Qjkfoqb37HNrGUUD5cj2q54gyp80w
    ==
    """
    And file "m2" is provided
    """
    hMQVVspFGh7NxCqLf7DVto3OxgDLn9n9gOCjOj
    EOYhi/VwZ3adFsbBL+zLZxYNdkKLNWCNRktRwp
    nWriEsW1uDnVt2LbxSLvjvRKbR/hyvpY+0UUZF
    S6wCWQjGyxUydDxQ88jNM5dY58/1nxsd04I3n3
    Mt97SuqwBN1+4VS3SsqtbR0GU1C7ODkPoCeGVd
    3PNkGHPgbT7QzMwxl63Pl3i/sp0I2/gqSnKu5C
    DS7e2WELz0hfiOJ2v2RvIon2EEbPwx1/6zxZlM
    hHuGXHNZKDtyqe6Dd+EIjpwhQFW3eH7fDIirRb
    aPPXAYsoypS5eFD3mIWUs4yVOH9ykkdKQ9FNwg
    ==
    """

  Scenario: Success:discovering the vulnerability
  https://fluidattacks.com/web/en/blog/save-the-world/
    When I inspect the PEM certificates
    Then I get the following results for the three files
    """
    $ openssl rsa -pubin -in files/clef0_pub.pem -text -noout -modulus
      Public-Key: (2048 bit)
      Exponent:
        3
      Modulus:
        BED09481E6C5438DEA330688659FB0DDA3B934924B86D522F131D41A4FDD49CA
        066346BA716D2B2AE6B0AA655EA1C72FC78672532E21B4EA5E268AA321B8A31C
        5FFAB1A0F7E83BC37892999729D9BA322465237FCB00AC683B16CA9AA40C8449
        6857C6018D8FC7C58F397D0656E8B6C85553F9A14F61965FECE0B85EEEE99B81
        28BF8B4CFFE25BE78A4B269944320138D45F4A16C84192AB5895FC64A4F2796A
        87234B48C46F2DF8719F28EFDAD022118652E3D25E44DA3E51FC13344E1C3ABD
        75E40C624949B29375BAB36E1FF9A692DF39EADCA179F26B4417CD5D71201F7D
        EEF7BD0533394157EB9CD292602C481448C3C20A36E189D7733B12AC39888F3B
    $ openssl rsa -pubin -in files/clef1_pub.pem -text -noout -modulus
      Public-Key: (2048 bit)
      Exponent:
        3
      Modulus:
        BA05F0CCE54B987C3EDADD20FE386AFC91BBA05F6BC9592D5EE3D4FD430D7DC3
        1503F5857A6C75695B46AA93FF792C55C3F7FC814A59B50F01037C7A66BD9A8A
        B2813B459FC7604F0CBF3E7B4AECA875B6C080685759F84E0956A276A9F789DF
        9A6CF5D32ED0ED28D05730D068089FBFA2CE6C4DAC9A2B82090A1B84019A3F69
        9E0D26B8E0CD19B90980970D3231BF9BE17C12E970B05F4252D98B78470E75C6
        1B4157489D41CDDA72B2E0EC9F7E310ADFB8BFF29DE344654CBFEE7243280B1F
        821FE5C90239A850AA4BE43D50C4202352CC97B211D7CB728A142CF2DF7AD739
        99119D478DFB676A943C5B7CAE30BA0E1A274E23D4CC635504B4B454C717F621
    $ openssl rsa -pubin -in files/clef2_pub.pem -text -noout -modulus
      Public-Key: (2048 bit)
      Exponent:
        3
      Modulus:
        ABC757E220D1F0DA46D017D0542273FAAEACA99D68EA3B400F99752B5DEBDB67
        AE3107D8F491F917BA079E14708F702FC8FC1A6189FC1F41EBA334482695642A
        FC8083D53DDA77B0B58C251926ED779446A137BBC4EFE3572B0D61DFC1A524C2
        C7891A6DE6BDA5B15B1604284CCA98B97F6E60974CB17F0A5B1A17BEF2007E02
        FA8A8BF0E0269DE44DAC25C7C7826CBAC2019435373E887658E5FAF733E08AE0
        4A052FA51447FFFB3B9EEDF7FEEC0C8E62AB0E51C516DB537EE93D9F780B20E1
        665D8E9E541D5E8DE9AA2EA1447B56D07770A2E5B72CAA0891988D97B77F3827
        4830814BD48E338800B0837FEF4135BAD88F5178941406CFDB831FE18171A13F
    """
    Given you send the same 'm' (message) to 'e' (exponent) recipients
    And 'c' is the original message
    And 'Ci' is the encrypted message for recipient i
    And 'Ni' is the modulus for recipient i
    And gcd(Ni, Nj) = 1 for all i, j
    And Ci = c % Ni for all i
    Then RSA is not safe
    And you can compute 'c' using the chinese remainder theorem
    And in other words, c = m^3 % (N1*N2*N3)
    But c = m^3, because RSA conditions
    Then just take the cubic root of a big number "c" and it's done

  Scenario: Success:hastaads broadcast attack
    Given I know the vulnerability
    Then I prepare data to exploit it
    When I turn the base64 encoded encrypted messages to integers
    And the modulus as hexadecimal string to integer
    """
    $ python
    >>> import base64
    >>> C1 = int(base64.b64decode(m0_as_string).encode('hex'), 16)
    ... some really big number ...
    >>> C2 = int(base64.b64decode(m1_as_string).encode('hex'), 16)
    ... some really big number ...
    >>> C3 = int(base64.b64decode(m2_as_string).encode('hex'), 16)
    ... some really big number ...
    >>> N1 = int(clef0_pub_pem_modulus_in_hex, 16),
    ... some really big number ...
    >>> N2 = int(clef1_pub_pem_modulus_in_hex, 16),
    ... some really big number ...
    >>> N3 = int(clef2_pub_pem_modulus_in_hex, 16),
    ... some really big number ...
    """
    Then I can run a common modulus attack
    """
    $ # take find_cube_root(val) from here
    $ #   https://stackoverflow.com/questions/23621833/is-cube-root-integer
    $ # take chinese_remainder(values, modulos) from here
    $ #   https://rosettacode.org/wiki/Chinese_remainder_theorem#Python
    $ # take mul_inv(vala, valb) from here
    $ #   https://rosettacode.org/wiki/Chinese_remainder_theorem#Python
    >>> matn = [N1, N2, N3]
    >>> mata = [C1, C2, C3]
    >>> valc = chinese_remainder(matn, mata)
    >>> valm = find_cube_root(valc)
    >>> print format(valm, 'x').decode('hex')
    Bravo! Pour valider le challenge, la clef est NoSpamWithRSA!
    Well done, the key for th challenge is NoSpamWithRSA!
    """
    When I enter "NoSpamWithRSA!" as flag
    Then I solve the challenge
