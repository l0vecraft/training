# Version 2.0
# language: en

Feature:  Cracking - Root Me
  Site:
     www.root-me.org
  Category:
    Cracking
  User:
    badwolf10
  Goal:
    Crack the password of a elf binary application

  Background:
  Hacker's software
    | <Software name>    |       <Version>      |
    | Ubuntu             | 18.04 LTS (x64)      |
    | Mozilla Firefox    | 63.0                 |
    | Visual Studio Code | 1.29.1               |
    | IDA                | 7.0.1180126          |
  Machine information:
    Given I the challenge url
    """
    https://www.root-me.org/en/Challenges/Cracking/ELF-No-software-breakpoints
    """
    And The challenge statement
    """
    Retrieve the password to validate this challenge.
    """
    Then I download the application binary "ch20.bin"

  Scenario: Success: Crack application password
    Given the executable "ch20.bin"
    Then I run it in console to see its behavior
    """
    $ Welcome to Root-Me Challenges
    Pass: xx
    Try again!
    """
    Then I read the file to see its general organization
    """
    $readelf -S ch20.bin
    [Nr] Name              Type        Addr     Off    Size   ES Flg Lk Inf Al
    [ 0]                   NULL        00000000 000000 000000 00      0   0  0
    [ 1] .text             PROGBITS    08048080 000080 0000a3 00  AX  0   0 16
    [ 2] .data             PROGBITS    08049124 000124 0000a3 00  WA  0   0  4
    [ 3] .note.gnu.gold-ve NOTE        00000000 0001c8 00001c 00      0   0  4
    [ 4] .shstrtab         STRTAB      00000000 0001e4 00002e 00      0   0  1
    """
    And I see the block ".text" is the executable part with flag "AX"
    Then I use IDA to open and disassemble "ch20.bin"
    And I start checking the generated assembler code
    And I notice is the assembler reading user input into "8049188" position
    """
    mov     ecx, offset aWelcomeToRootM; "Welcome to Root-Me
    Challenges\r\nPass: "
    mov     edx, 26h
    int     80h             ;$!
    mov     eax, 3
    xor     ebx, ebx
    mov     ecx, offset unk_8049188
    mov     edx, 33h
    int     80h             ;$!
    """
    Then I notice a call to a subroutine "sub_8048115"
    """
    xor     ecx, ecx
    mov     eax, offset start
    mov     ebx, 8048123h
    call    sub_8048115
    """
    And I check the subroutine [evidence](checksum.png)
    """
    sub_8048115 proc near
    sub     ebx, eax
    xor     ecx, ecx
    loc_8048119:
    add     cl, [eax]
    rol     ecx, 3
    inc     eax
    dec     ebx
    jnz     short loc_8048119
    retn
    sub_8048115 endp
    """
    And I see it iterates through the program block
    And I see it continuously adds the value of each instruction to a register
    And The register is rotated 3 bits every interation
    Then I deduce it is computing some kind of checksum over the code
    And It may prevent code alteration
    Then I write a python code doing the same operation
    And I check the next piece of code [evidence](main-loop.png)
    """
    loc_80480C1:
    mov     eax, offset unk_8049155
    mov     ebx, offset unk_8049188
    ror     edx, 1
    mov     al, [eax+ecx-1]
    mov     bl, [ebx+ecx-1]
    xor     al, bl
    xor     al, dl
    jnz     short loc_80480F6
    """
    And the next piece of code
    """
    dec     ecx
    jnz     short loc_80480C1
    """
    And I notice the program iterates through a block with the user input data
    And It also iterates through a block of validation data
    And It uses the checksum along with xor operations to validate password
    Then I write a python code emulating the same operation
    And I modify it to predict the value user must have input
    """
    al : value at each iteration over validation data block
    bl : value at each iteration over user input data block
    The prrogram validates:
    (al ^ bl) ^ dl = 0
    therefore we can find the expected bl for the equality to hold
    (al ^ bl) ^ dl ^ dl = 0 ^ dl
    al ^ bl = dl
    bl = al ^ dl
    """
    Then I run the script with program and validation blocks data as inputs
    And I find the password
    """
    HardW@re_Br3akPoiNT_r0ckS
    """
    And I solve the challenge
