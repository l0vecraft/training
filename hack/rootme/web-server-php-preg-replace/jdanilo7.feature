## Version 2.0
## language: en

Feature:  Web Server - Root Me
  Site:
    www.root-me.org
  Category:
    Web Server
  User:
    jdanilo7
  Goal:
    Read flag.php exploiting the e modifier in preg_replace()

  Background:
  Hacker's software
    |     <Software name>      |    <Version>    |
    |     Firefox Quantum      |      65.0.2     |
  Machine information:
    Given The challenge url
    """
    http://challenge01.root-me.org/web-serveur/ch37/index.php
    """
    And The challenge statement
    """
    Read flag.php.
    """
    Then I am asked to read a file exploiting the e modifier for php regex

  Scenario: Success: Use the e modifier and the file_get_contents() function
    Given I click the "Start the challenge" button
    Then I am redirected to a webpage with a regex evaluator
    And there is a field for a pattern, one for a replacement string
    And one for the string that must be modified
    And there is also a "Submit" button
    Given I enter "/a/e" as a pattern
    And "file_get_contents('flag.php')" as a replacement string
    And "a" as the string to be modified
    And I click the "Submit" button
    Then I get the following output
    """
    <?php $flag="pr3g_r3pl4c3_3_m0d1f13r_styl3"; ?>
    """
    And I validate the flag on the challenge website
    And it is accepted
