# No config file found, using default configuration
# --------------------------------------------------------------------
# Your code has been rated at 10.00/10 (previous run: 10.00/10, +0.00)

"""
Script to extract frames from a gif file
"""

from PIL import Image

# open gif
GIF = Image.open('gif_animado_i.gif')

# iterate over frames
for frame in range(0, GIF.n_frames):
    # get frame at this position
    GIF.seek(frame)
    # save current frame
    GIF.save(str(frame)+".gif")
print "Images saved"

# $ python frames.py
# Images saved
