Feature: Solve Leonardo challenge
  from site World of Wargame
  logged as EdPrado4

Background:
  Given I have access to Internet
  And I have Ubuntu 16.04 LTS OS

Scenario: Challenge solved
  Given The challenge presented
  When I recognize most of the images
  And I search in google images for their names
  Then I solve the challenge
