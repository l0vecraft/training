Feature:
  Solve Zebra
  From wechall site
  Category Training, Encoding, Stegano

Background:
  Given I am running Windows 8.1 (64bit)
  And I am using Google Chrome Version 70.0.3538.77 (Official Build) (64-bit)

Scenario:
  Given the challenge URL
  """
  http://www.wechall.net/challenge/Hirsch/Zebra/index.php
  """
  Then I opened that URL with Google Chrome
  And I see the problem statement
  """
  The Zebra
  Help! A zebra escaped from its enclosure. But where is it now?
  """
  Then I opened the console debug of browser
  And I analized code looking for the answer
  And I noted a hidden column with a value
  And I put this value as answer but the answer was wrong.

Scenario: Failed Attempt number 1
  Given the challenge url
  Then I open it with Google Chrome
  And I tried to put nothing as answer but the answer was wrong.

Scenario: Failed Attempt number 2
  Given the challenge url
  Then I open it with Google Chrome
  And I discovered that the zebra stripes are a codebar
  Then I passed the zebra image to this barceode reader online
  """
  http://www.ibscanner.com/online-barcode-scanner
  """
  Then barcode reader gave me a number
  And I put this number as answer but the answer was wrong.

Scenario: Successful solution
  Given the challenge url
  Then I open it with Google Chrome
  And I opened paint to cut out the image
  And I leave only the codebar
  Then I roted the image 180 degree and save it.
  Then I passed the image for the barcode reader
  And gave me the follow value
  """
  the answer is saFFari
  """
  Then I put as answer saFFari and the answer is correct.
