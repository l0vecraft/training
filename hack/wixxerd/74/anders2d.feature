## Version 2.0
## language: en

Feature: 74-programming-wixxerd
  Code:
    74
  Site:
    https://www.wixxerd.com
  Category:
    programming
  User:
    anders2d
  Goal:
    Solve capcha equation in 2 seconds or less.

  Background:
  Hacker's software:
    | <Software name>   |  <Version>    |
    | Windows           | 10.0.1809     |
    | Chrome            | 76.0.3809.132 |

  Scenario: fail:try-to-solve-directly
    Given I can put the response manually
    When I try to solve the capcha equation
    Then the response is incorrect

  Scenario: fail:search-in-source-code
    Given I have access to HTML code
    When I search captcha numbers
    Then I notice that captcha numbers are encrypted
    And I try to use symmetric key decryptography
    But I don't can catch the flag because the decryption is ambiguous

  Scenario: fail:try-decrypt-capcha-with-a-hint
    Given I have access to HTML code
    When I see the code and I found that exist the next HTML line
    """
    <input type="hidden" name="c" value="49" id="c">
    """
    Then after several attempts watching the C variable behavior
    And I notice that if C variable is  48, the captcha numbers are equal
    But if C number is different to 48, the captcha number is different
    And I use 48 number as reference to use symmetric key decryptography
    And I notice that the captcha decryption is succefull
    But the attempts are very slow to catch the flag

  Scenario: success:try-use-javascript-in-chrome-console
    Given I have use JavaScript in chromer console
    When I use the next JavaScript code:
    """
    A = a.value
    B = b.value
    C = c.value
    dif = 48 - parseInt(C)

    A = reWrite( A , dif)
    B = reWrite( B , dif)

    result = parseInt(A)*parseInt(B)

    function reWrite(number,value){
        if (value == 0 ) { return number}
        newNumber = ""
        for (i in number){
            int = number.charCodeAt(i) + value
            str = String.fromCharCode(int)
            newNumber = newNumber + str
        }
        return newNumber
    }
    res.value = result
    submit[0].click()
    """
    When I run the code several times
    Then I catch the flag
