## Version 2.0
## language: en

Feature: 71-programming-wixxerd
  Code:
    71
  Site:
    https://www.wixxerd.com
  Category:
    programming
  User:
    anders2d
  Goal:
     Print each name, password and role sorting      ascending by name

  Background:
  Hacker's software:
    | <Software name>   |  <Version>    |
    | Windows           | 10.0.1809     |
    | Chrome            | 76.0.3809.132 |
  Machine information:
    Given I am accessing to input to run queriesx|
    And PostresSQL version is PostgreSQL 9.3.21
    And is running on Ubuntu 4.8.4

  Scenario: Fail:normal-SQL
    Given I can run select query
    When I run the next queries
    """
    select * from users;
    select * from roles;
    select * from user_roles;
    """
    Then the result set of each is
    """
    --users--
    ID    PASSWORD    UNAME
    1    5922B67C56DF1BA2339CB7FC43F9F29D    Admin
    2    c55cbda26407b5d2650d530afd387eed    Cletus
    3    4155894047994994173745FBD8176A2E    Larry
    4    1870180768DBF817D4A213060CFC7AE9    Joe
    5    5F4DCC3B5AA765D61D8327DEB882CF99    Linda
    6    84169a8d5b3289e8ece00d7735081b53    Sarah
    --user_roles--

    ID    ROLEID    USERID
    1    1    2
    2    3    1
    3    2    5
    4    4    4
    5    5    6
    6    4    3
    --roles--
    DESCRIPTION    ID    NAME
    Cleaner of all things unsanitary.    1    Janitor
    Cat Herder    2    Project Manager
    Refuses to bathe    3    Software Developer
    Not even the graphics designers like these guys    4    COBOL Programmer
    Not worth the trouble    5    Legal Intern
    """
    And I got the schema of this database
    Then I run the next query
    """
    select users.UNAME, users.PASSWORD , roles.NAME
    from users
    inner join user_roles on (user_roles.USERID = users.ID)
    inner join roles on (roles.ID = user_roles.ROLEID)
    order by users.UNAME asc
    """
    Then show then next message
    """
    Testing your query...
    Checking return columns... Fail!
    Here's what your query produced...
    """
    And I could not capture the flag

  Scenario: Success:alias-column-order
    Given I got the schemea and query
    When I use alias to sorted alphabetically users.UNAME and roles.NAME
    Then I run the next query
    """
    select users.UNAME as Name , users.PASSWORD , roles.NAME  AS Role
    from users
    inner join user_roles on (user_roles.USERID = users.ID)
    inner join roles on (roles.ID = user_roles.ROLEID)
    order by users.UNAME asc
    """
    Then I solved the challenge
    And I caught the flag
